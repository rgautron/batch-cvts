from gym_dssat_pdi.envs.utils import utils as dssat_utils
from batch_utils import rendering
from batch_utils import utils as batch_utils
from batch_utils import parallel_training
from classes.policies import Fertilizer_Policy
from classes.batch_generator import Batch_Generator
from classes.batch_env import Batch_Env, fill_default_action_value_dict
from classes.meta_batch_bandit import Meta_Batch_Bandit, B_CVTS_BATCH, ETC_CVAR_BATCH
from classes.template_maker import Template_Maker
import gym
import os
from pathlib import Path
import itertools

os.environ['OPENBLAS_NUM_THREADS'] = '1'  # avoid error in clusters
import sys
import argparse
from copy import deepcopy
import numpy as np
import faulthandler

faulthandler.enable()
import logging
from tqdm import tqdm
import time
import pprint

import pdb
import pandas as pd


def get_regret_parallel(meta_bandit_params, horizon, replications):
    try:
        batch_bandit = Meta_Batch_Bandit(**meta_bandit_params)
        batch_bandit.close()
        result_dic = parallel_training.get_parallel_regret_(bandit=batch_bandit,
                                                            horizon=horizon,
                                                            replications=replications,
                                                            )
        return result_dic
    except Exception as e:
        logging.exception(e)
        sys.exit()
    finally:
        try:
            batch_bandit.close()
        except:
            pass


def get_regret_monocore(meta_bandit_params, horizon, replications, seeds=None):
    try:
        regrets = []
        actions = []
        farmer_regrets = []
        rewards = []
        contexts = []
        efficiencies = []
        meta_bandit = Meta_Batch_Bandit(**meta_bandit_params)
        for replication in tqdm(range(replications), desc='replications', position=0):
            if seeds is None:
                seed_replication = 1234
            else:
                seed_replication = seeds[replication]
            meta_bandit.reset(seed=seed_replication)
            for _ in tqdm(range(horizon - 1), desc='horizon', position=1, colour='red'):
                meta_bandit.step()
            for context in [*meta_bandit.bandit_dic]:
                print(meta_bandit.bandit_dic[context].pulls)
                print(meta_bandit.bandit_dic[context].criteria)
                print(meta_bandit.pulls)
            regret = meta_bandit.get_regret()
            rewards.append(meta_bandit.batch_reward_history)
            contexts.append(meta_bandit.batch_context_history)
            efficiencies.append(meta_bandit.batch_efficiency_history)
            actions.append(meta_bandit.batch_action_history)
            farmer_statistics = meta_bandit.get_farmer_statistics()
            for context in [*farmer_statistics]:
                farmer_regrets.extend(farmer_statistics[context]['true_regret'])
            regrets.append(regret)
        res_dic = {}
        res_dic['actions'] = actions
        res_dic['regret'] = regrets
        res_dic['farmer_regret'] = farmer_regrets
        res_dic['rewards'] = rewards
        res_dic['contexts'] = contexts
        res_dic['efficiencies'] = efficiencies
        return res_dic
    except Exception as e:
        logging.exception(e)
    finally:
        try:
            meta_bandit.close()
        except:
            pass


def get_best_performing_policies(batch_env, df_saving_path):
    statistic_dic = batch_env.statistics
    soils = [*statistic_dic]
    metrics_by_feature = {'cumsumfert': ['mean', 'std'],
                          'grnwt': ['cvar', 'mean', 'std'],
                          'efficiency': ['mean', 'std'],
                          'reward': ['cvar', 'mean', 'std'], }
    decimals = {'cumsumfert': 1,
                'grnwt': 0,
                'efficiency': 1,
                'reward': 0}
    best_performing_policies = {}
    for soil in soils:
        soil_values = statistic_dic[soil]
        reward_cvars = soil_values['all']['reward']['cvar']
        best_performing_policies[soil] = np.argmax(reward_cvars)
    df_dic = {}
    for soil in soils:
        df_dic[soil] = {}
        soil_values = statistic_dic[soil]
        best_performing_policy_index = best_performing_policies[soil]
        df_dic[soil]['best_policy'] = best_performing_policy_index
        for feature in [*metrics_by_feature]:
            for metric in metrics_by_feature[feature]:
                value = soil_values['all'][feature][metric][best_performing_policy_index]
                key_name = f'{feature}_{metric}'
                decimal_value = decimals[feature]
                value = np.around(value, decimals=decimal_value)
                if decimal_value == 0:
                    value = int(value)
                df_dic[soil][key_name] = value
    df = pd.DataFrame.from_dict(df_dic, orient='index')
    df.index.name = 'soil'
    df.to_csv(df_saving_path)


def get_performance_all_policies(batch_env, df_saving_path):
    statistic_dic = batch_env.statistics
    metrics_by_feature = {'cumsumfert': ['mean', 'std'],
                          'grnwt': ['cvar', 'mean', 'std'],
                          'efficiency': ['mean', 'std'],
                          'reward': ['cvar', 'mean', 'std'], }
    decimals = {'cumsumfert': 1,
                'grnwt': 0,
                'efficiency': 1,
                'reward': 0}
    df_dic = {}
    df_dic['soil_cultivar_action_index'] = []
    for soil in [*statistic_dic]:
        for cultivar in [*statistic_dic[soil]]:
            df_dic['soil_cultivar_action_index'].extend(
                [f'{soil}_{cultivar}_action_{action_index}' for action_index in range(batch_env.n_actions)])
            for feature in [*metrics_by_feature]:
                for metric in metrics_by_feature[feature]:
                    if f'{feature}_{metric}' not in df_dic:
                        df_dic[f'{feature}_{metric}'] = []
                    values = statistic_dic[soil][cultivar][feature][metric]
                    values = np.around(values, decimals=decimals[feature]).tolist()
                    df_dic[f'{feature}_{metric}'].extend(values)
    df = pd.DataFrame.from_dict(df_dic, orient='columns')
    df.to_csv(df_saving_path, index=False)
    print(df_dic)
    pdb.set_trace()


if __name__ == '__main__':
    #######################
    # ARG FROM SHELL OPTION
    #######################
    args_from_shell = True

    print(f'\n\n !!! ARGS FROM SHELL: {args_from_shell} !!! \n\n')

    parser = argparse.ArgumentParser()
    parser.add_argument('--alpha', '-a', type=str, const=1, nargs='?', default=0.3,
                        help='the level of the CVaR in (0,1]')
    parser.add_argument('--populationsize', '-ps', type=str, const=1, nargs='?', default='True',
                        help='if having a larger farmer population than volunteers')
    parser.add_argument('--minbatchsize', '-mnbs', type=str, const=1, nargs='?', default='True',
                        help='if random batch size')
    args = parser.parse_args()
    assert args.populationsize in ['True', 'False']
    population_size_ = True if args.populationsize == 'True' else False
    assert args.minbatchsize in ['True', 'False']
    min_batch_size_ = True if args.minbatchsize == 'True' else False
    alpha = float(args.alpha)
    assert 0 < alpha <= 1

    ######################################
    ############ EXP SETTINGS ############
    ######################################
    g5k = True
    # g5k = False
    # verbose = True
    verbose = False
    make_fileX_templates_bool = True
    # make_fileX_templates_bool = False
    # compute_samples = True
    compute_samples = False
    # print_action_statistics = True
    print_action_statistics = False
    # render_env = True
    render_env = False
    render_env_ridge_plot = True
    # render_env_ridge_plot = False
    # run_bandits = True
    run_bandits = False
    render_regret = True
    # render_regret = False
    # prior = True
    prior = False
    # sliding_window = True
    sliding_window = False
    log = True
    # log = False
    multicore = True
    # multicore = False
    # random_inital_nitrogen = True
    random_inital_nitrogen = False
    # contextless = True
    contextless = False
    fair_exploration = True
    # fair_exploration = False
    # random_cultivar_list = [f'IM000{i}' for i in range(1, 4)]
    random_cultivar_list = None
    saving_policy_statistics = True
    # saving_policy_statistics = False

    contexts = [
        'ITML840101',
        'ITML840102',
        'ITML840103',
        'ITML840104',
        'ITML840105',
        'ITML840106',
        'ITML840107',
    ]

    initial_nitrogen_dic = {
        'ITML840101': {'snh4': .5, 'sno3': .05},
        'ITML840102': {'snh4': .3, 'sno3': 1.5},
        'ITML840103': {'snh4': .3, 'sno3': .7},
        'ITML840104': {'snh4': .5, 'sno3': 2},
        'ITML840105': {'snh4': .2, 'sno3': 1},
        'ITML840106': {'snh4': .1, 'sno3': .7},
        'ITML840107': {'snh4': .5, 'sno3': 1.8},
    }

    all_soils_distribution = {
        'ITML840101': .07,
        'ITML840102': .09,
        'ITML840103': .21,
        'ITML840104': .04,
        'ITML840105': .24,
        'ITML840106': .27,
        'ITML840107': .08,
    }

    bounds_dict = {
        'efficiency': [0, 70],
        'grnwt': [0, 8000],
        'reward': [-1000, 4000],
    }

    reward_feature_layout = {
        'efficiency': 'nitrogen use efficency (kg/kg)',
        'grnwt': 'dry grain yield (kg/ha)',
        'reward': 'yield excess (kg/ha)',
    }

    reward_limit_efficiency = 15


    def reward_function(interaction):
        return reward_function_(interaction, **reward_function_args)


    ### Normalization for experiments on subsets
    soils_distribution = {}
    cumulated_probability = 0
    for soil in contexts:
        cumulated_probability += all_soils_distribution[soil]
    for soil in contexts:
        soils_distribution[soil] = all_soils_distribution[soil] / cumulated_probability
    ### /Normalization for experiments on subsets

    rain_threshold = 200
    reward_feature = 'reward'
    # reward_feature = 'grnwt'
    # reward_feature = 'efficiency'

    if run_bandits:
        reward_feature = 'reward'

    if saving_policy_statistics:
        assert reward_feature == 'reward'

    if reward_feature == 'reward' or reward_feature == 'grnwt':
        render_env_mean_bars = False
        render_env_cvar_bars = True
    else:
        render_env_mean_bars = True
        render_env_cvar_bars = False


    def clustering_function(context):
        return context


    # TODO: add initial nitrogen conditions by soil
    clusters = list(set([clustering_function(context) for context in contexts]))
    fileX_prefix = 'GENERIC'
    population_size = None
    min_batch_size = None

    if g5k:
        multicore = True
        n_samples = 1 * int(1e5)
        soils_to_select = contexts
        replications = 80 * 12
        horizon = 20
        if population_size_:
            population_size = 500  # larger population of farmers
        max_batch_size = 350
        if min_batch_size_:
            min_batch_size = 250
        dssat_path = f'/opt/dssat_pdi/'
    else:
        n_samples = 1 * int(1e5)
        # n_samples = 5 * int(1e1)
        soils_to_select = ['ITML840104', 'ITML840107']
        # soils_to_select = contexts
        if multicore:
            replications = 8
        else:
            replications = 2
        horizon = 6
        if population_size_:
            population_size = 80
        max_batch_size = 70
        if min_batch_size_:
            min_batch_size = 50
        dssat_path = f'{Path.home()}/dssat/'

    if not min_batch_size_:
        if g5k:
            max_batch_size = 300
        else:
            max_batch_size = 60

    bandit_names = [
        'B_CVTS_BATCH',
        'ETC_CVAR_BATCH_3',
        'ETC_CVAR_BATCH_5',
    ]
    bandit_names_plot_dic = {
        'B_CVTS_BATCH': 'BCB',
        'ETC_CVAR_BATCH_3': 'ETC_3',
        'ETC_CVAR_BATCH_5': 'ETC_5',
    }
    bandit_instance_dic = {
        'B_CVTS_BATCH': B_CVTS_BATCH,
        'ETC_CVAR_BATCH_3': ETC_CVAR_BATCH,
        'ETC_CVAR_BATCH_5': ETC_CVAR_BATCH,
    }
    ingenos = [
        # 'GH0010',
        'IM0001'
    ]
    delta_ci = .1
    # default_action_index = 3
    default_action_index = None
    # efficiency_threshold = 25
    efficiency_threshold = None
    rainfall_threshold = 200
    nsi_threshold = .8
    # initial_batch_action_probability = [0, 0, 0, 1]
    initial_batch_action_probability = None
    # TODO: NOW DEFAULT VALUES TO PROVIDE DIRECTLY e.g. {cluster0: {'action_index0': default_value}}
    default_action_values_dict = None
    if prior:
        random_target_contexts = np.random.choice(soils_to_select, size=len(soils_to_select), replace=False)
        prior_mapping = {source_context: random_target_context for source_context, random_target_context
                         in zip(soils_to_select, random_target_contexts)}
    else:
        prior_mapping = None
    wsta_dic = {'GENERIC': 'NTAR'}
    soil_file_path = f'./dssat_files/auxiliary_files/IT.SOL'
    wsta = wsta_dic[fileX_prefix]
    if wsta == 'NTAR':
        auxiliary_file_paths = [f'./dssat_files/auxiliary_files/{file}' for file in ['NTAR.CLI', 'NTAR6501.WTH',
                                                                                     'IT.SOL']]
    else:
        auxiliary_file_paths = None
    if log:
        log_saving_path = 'logs/dssat-pdi.log'
    else:
        log_saving_path = None
    if sliding_window:
        n_batch_window = 10  # number of batch of sliding window
    else:
        n_batch_window = None
    if prior:
        n_prior = max_batch_size  # number of prior samples
    else:
        n_prior = None
    n_init_context_batch = int(max_batch_size)  # number of samples for bandit init
    sample_saving_prefix = './output/'
    # TODO: saving min batch size and pop size
    best_action_statistic_csv_saving_path = f'{sample_saving_prefix}best_action_dssat_statistics_n_{n_samples}.csv'
    all_actions_statistic_csv_saving_path = f'{sample_saving_prefix}all_actions_dssat_statistics_n_{n_samples}.csv'
    regret_saving_path = f'./output/regret_B_{max_batch_size}_H_{horizon}_rep_{replications}_A_{100 * alpha:.0f}p' \
                         f'_ps_{population_size_}_rb_{min_batch_size_}.pkl'
    regret_render_saving_path = f'./render/regret_plot_B_{max_batch_size}_R_{replications}_H_{horizon}_A_' \
                                f'{100 * alpha:.0f}_100_ps_{population_size_}_rb_{min_batch_size_}.pdf'
    farmer_regret_render_saving_path = f'./render/farmer_regret_plot_B_{max_batch_size}_R_{replications}_H_{horizon}_A_' \
                                       f'{100 * alpha:.0f}_100_ps_{population_size_}_rb_{min_batch_size_}.pdf'
    farmer_empirical_cvar_render_saving_path = f'./render/farmer_empirical_cvar_plot_B_{max_batch_size}_R_{replications}' \
                                               f'_H_{horizon}_A_{100 * alpha:.0f}_100_ps_{population_size_}_rb_{min_batch_size_}.pdf'
    farmer_efficiency_render_saving_path = f'./render/farmer_efficiencies_plot_B_{max_batch_size}_R_{replications}' \
                                           f'_H_{horizon}_A_{100 * alpha:.0f}_100_ps_{population_size_}_rb_{min_batch_size_}.pdf'
    actions_render_saving_path_prefix = f'./render/actions/actions_plot_B_{max_batch_size}_R_{replications}' \
                                        f'_H_{horizon}_A_{100 * alpha:.0f}_100_ps_{population_size_}_rb_{min_batch_size_}'
    dirs = ['./logs', './render', './output', './dssat_files/child_fileX_templates',
            './render/actions', './configs']
    for dir in dirs:
        dssat_utils.make_folder(dir)
    try:
        for file in os.scandir('logs'):
            os.remove(file.path)
    except:
        pass

    ##############################################
    ############ TEMPLATE PREPARATION ############
    ##############################################
    filling_dictionaries = {
        context: {ingeno: {'id_soil': context, 'wsta': wsta, 'ingeno': ingeno} for ingeno in ingenos} for context in
        contexts}  # ingeno specified
    # later
    child_template_saving_paths = {
        context: {ingeno: f'./dssat_files/child_fileX_templates/{fileX_prefix}_{context}_{ingeno}.jinja2' for ingeno in
                  ingenos} for context in contexts}
    parent_fileX_template_path = f'./dssat_files/parent_fileX_templates/{fileX_prefix}_parent.jinja2'
    template_maker = Template_Maker(parent_fileX_template_path=parent_fileX_template_path,
                                    child_template_saving_paths=child_template_saving_paths,
                                    soil_file_path=soil_file_path,
                                    filling_dictionaries=filling_dictionaries,
                                    random_inital_nitrogen=random_inital_nitrogen,
                                    random_cultivar_list=random_cultivar_list,
                                    initial_nitrogen_dic=initial_nitrogen_dic,
                                    dssat_path=dssat_path)
    if make_fileX_templates_bool:
        template_maker.make_child_templates()
    # seed = np.random.randint(1, 9999999)
    seed = 123
    np.random.seed(seed)
    run_dssat_location = f'/opt/dssat_pdi/run_dssat'
    env_args = {
        'run_dssat_location': run_dssat_location,
        'log_saving_path': log_saving_path,
        'mode': 'fertilization',
        'random_weather': True,
        'experiment_number': 1,
        'auxiliary_file_paths': auxiliary_file_paths,
        'seed': seed,
    }
    all_env_args = {}
    for context in contexts:
        all_env_args[context] = {}
        for ingeno in ingenos:
            env_args_cp = deepcopy(env_args)
            env_args_cp['fileX_template_path'] = child_template_saving_paths[context][ingeno]
            all_env_args[context][ingeno] = env_args_cp

    ############################################
    ############ ENV INITIALIZATION ############
    ############################################
    fertilizer_policy_args = {'rainfall_threshold': rainfall_threshold,
                              'nsi_threshold': nsi_threshold
                              }
    fertilizer_policy = Fertilizer_Policy(**fertilizer_policy_args)
    n_actions = len([*fertilizer_policy.policy_dic])
    batch_generator_args = {'atomic_contexts': soils_to_select,
                            'max_batch_size': max_batch_size,
                            'clustering_function': clustering_function,
                            'n_batch_window': n_batch_window,
                            'n_actions': n_actions,
                            'default_action_values_dict': default_action_values_dict,
                            'population_size': population_size,
                            'min_batch_size': min_batch_size,
                            'ingenos': ingenos,
                            'atomic_context_probability_dict': soils_distribution,
                            'seed': seed,
                            'init': False
                            }
    batch_generator = Batch_Generator(**batch_generator_args)
    if contextless:
        batch_generator_args = deepcopy(batch_generator_args)
        batch_generator_args_['clustering_function'] = lambda context: 'UNIQUE_CONTEXT'
        contextless_batch_generator = Batch_Generator(**batch_generator_args_)
    all_envs = {
        soil_id: {ingeno: gym.make('gym_dssat_pdi:GymDssatPdi-v0', **all_env_args[soil_id][ingeno]) for ingeno in
                  ingenos} for soil_id in soils_to_select}
    batch_env_args = {
        'batch_generator': batch_generator,
        'env_dic': all_envs,
        'policy': fertilizer_policy,
        'reward_feature': reward_feature,
        'bounds_dict': bounds_dict,
        'alpha': alpha,
        'template_maker': template_maker,
        'sample_saving_prefix': sample_saving_prefix,
        'n_samples': n_samples,
        'seed': seed,
        'limit_efficiency': reward_limit_efficiency,
    }
    batch_env = Batch_Env(**batch_env_args)
    batch_env.get_dist_params(compute_samples=compute_samples)
    if saving_policy_statistics:
        get_performance_all_policies(batch_env=batch_env,
                                     df_saving_path=all_actions_statistic_csv_saving_path)
        get_best_performing_policies(batch_env=batch_env,
                                     df_saving_path=best_action_statistic_csv_saving_path)
    if print_action_statistics:
        batch_env.print_action_statistics()
    if render_env:
        try:
            if render_env_ridge_plot:
                batch_env.render_env_ridge_plot(saving_prefix='./render/',
                                                mean_bars=render_env_mean_bars,
                                                cvar_bars=render_env_cvar_bars,
                                                reward_feature_layout=reward_feature_layout)
            else:
                batch_env.render_env(saving_prefix='./render/',
                                     mean_bars=render_env_mean_bars,
                                     cvar_bars=render_env_cvar_bars,
                                     reward_feature_layout=reward_feature_layout)
        except Exception as e:
            logging.exception(e)
    if default_action_values_dict is not None:
        default_action_value_dict = fill_default_action_value_dict(default_action_values_dict, batch_env,
                                                                   batch_generator)
    else:
        default_action_value_dict = None

    ####################################
    ############ BANDIT RUN ############
    ####################################
    batch_env.close()
    if run_bandits:
        all_bandit_regret_dic = {}
        for index, bandit_name in enumerate(bandit_names):
            if fair_exploration:
                if bandit_name == 'B_CVTS_BATCH':
                    fair_exploration_ = True
                else:
                    fair_exploration_ = False
            else:
                fair_exploration_ = False
            if bandit_name == 'ETC_CVAR_BATCH_3':
                n_batch_explore = 3
            elif bandit_name == 'ETC_CVAR_BATCH_5':
                n_batch_explore = 5
            else:
                n_batch_explore = None
            monocore_seeds = np.random.choice(range(1000000), size=replications, replace=False)
            bandit_instance = bandit_instance_dic[bandit_name]
            bandit_params = {'n_batch_window': n_batch_window,
                             'default_action_index': default_action_index,
                             'initial_batch_action_probability': initial_batch_action_probability,
                             'default_action_value_dic': default_action_value_dict,
                             'n_batch_explore': n_batch_explore,
                             'feature_threshold': efficiency_threshold,
                             'horizon': horizon,
                             'seed': seed,
                             }
            ###########################
            #### CONTEXTUAL BANDIT ####
            ###########################
            meta_bandit_params = {'prior_mapping': prior_mapping,
                                  'n_prior': n_prior,
                                  'batch_env': batch_env,
                                  'bandit_params': bandit_params,
                                  'bandit_instance': bandit_instance,
                                  'fair_exploration': fair_exploration_,
                                  'init_bandits': False,
                                  }
            all_parameters_dict = {
                'env_args': env_args,
                'batch_generator_args': batch_generator_args,
                'batch_env_args': batch_env_args,
                'bandit_params': bandit_params,
                'meta_bandit_params': meta_bandit_params
            }
            saving_id = time.strftime("%Y%m%d_%H%M%S")
            parameters_saving_path = f'./configs/{bandit_name}_{saving_id}.config'
            pprint.pprint(all_parameters_dict)
            time.sleep(10)
            batch_utils.save_pprint_dicts(dict=all_parameters_dict, saving_path=parameters_saving_path)
            if multicore:
                context_raw_results = get_regret_parallel(meta_bandit_params=meta_bandit_params,
                                                          horizon=horizon,
                                                          replications=replications)
            else:
                context_raw_results = get_regret_monocore(meta_bandit_params=meta_bandit_params,
                                                          horizon=horizon,
                                                          replications=replications,
                                                          seeds=monocore_seeds)
            ############################
            #### CONTEXTLESS BANDIT ####
            ############################
            if contextless:
                batch_env_args['batch_generator'] = contextless_batch_generator
                if multicore:
                    contextless_raw_results = get_regret_parallel(meta_bandit_params=meta_bandit_params,
                                                                  horizon=horizon,
                                                                  replications=replications)
                else:
                    contextless_raw_results = get_regret_monocore(meta_bandit_params=meta_bandit_params,
                                                                  horizon=horizon,
                                                                  replications=replications,
                                                                  seeds=monocore_seeds)
                values = [context_raw_results, contextless_raw_results]
            else:
                values = [context_raw_results]
            bandit_names_ = [bandit_name]
            if contextless:
                bandit_names_.append(f'CL-{bandit_name}')
            raw_result_dic = {key: value for key, value in zip(bandit_names_, values)}
            info_dic = {'ingenos': ingenos, 'reward_feature': batch_env.reward_feature}
            all_bandit_regret_dic = {**all_bandit_regret_dic, **raw_result_dic, 'info': info_dic}
        batch_utils.save_object(obj=all_bandit_regret_dic, saving_path=regret_saving_path)
    if render_regret:
        if not run_bandits:
            all_bandit_regret_dic = batch_utils.load_object(regret_saving_path)
        #  regret normalized during the calculation
        bandit_names = [*all_bandit_regret_dic]
        label = 'cumulated YE CVAR regret (kg/ha)'
        # rendering.plot_multiple_regrets(replications=replications,
        #                                 horizon=horizon,
        #                                 raw_results=all_bandit_regret_dic,
        #                                 alpha=alpha,
        #                                 saving_path=regret_render_saving_path,
        #                                 batch_size=max_batch_size,
        #                                 reward_feature_layout=reward_feature_layout,
        #                                 y_label=label,
        #                                 bandit_names_plot_dic=bandit_names_plot_dic)
        # if not population_size_ and not min_batch_size_:
        #     rendering.plot_farmer_regrets(raw_results=all_bandit_regret_dic,
        #                                   alpha=alpha,
        #                                   saving_path=farmer_regret_render_saving_path,
        #                                   batch_size=max_batch_size,
        #                                   horizon=horizon,
        #                                   replications=replications,
        #                                   reward_feature_layout=reward_feature_layout,
        #                                   x_label=label,
        #                                   bandit_names_plot_dic=bandit_names_plot_dic)
        # rendering.plot_farmer_empirical_cvar(raw_results=all_bandit_regret_dic,
        #                                      alpha=alpha,
        #                                      saving_path=farmer_empirical_cvar_render_saving_path,
        #                                      batch_size=max_batch_size,
        #                                      horizon=horizon,
        #                                      replications=replications,
        #                                      bounds_dict=bounds_dict,
        #                                      delta_ci=delta_ci,
        #                                      reward_feature_layout=reward_feature_layout,
        #                                      y_label=f'empirical CVaR of YE (kg/ha)',
        #                                      bandit_names_plot_dic=bandit_names_plot_dic)
        # rendering.plot_farmer_efficiencies(raw_results=all_bandit_regret_dic,
        #                                    alpha=alpha,
        #                                    saving_path=farmer_efficiency_render_saving_path,
        #                                    batch_size=max_batch_size,
        #                                    horizon=horizon,
        #                                    replications=replications,
        #                                    efficiency_threshold=efficiency_threshold,
        #                                    supp=bounds_dict['efficiency'],
        #                                    delta_ci=delta_ci)
        rendering.plot_action_distribution(raw_results=all_bandit_regret_dic,
                                           alpha=alpha,
                                           saving_path_prefix=actions_render_saving_path_prefix,
                                           batch_size=max_batch_size,
                                           horizon=horizon,
                                           n_actions=n_actions,
                                           statistics=batch_env.statistics,
                                           gaps=batch_env.true_gaps,
                                           efficiency_threshold=efficiency_threshold,
                                           replications=replications,
                                           bandit_names_plot_dic=bandit_names_plot_dic)
