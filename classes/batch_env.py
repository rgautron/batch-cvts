from pprint import pprint
import numpy as np
import multiprocessing as mp
from cvarBandits.utils.estimators import moment_empirical_cvar
from batch_utils.utils import load_json, dic_to_json
import atexit
from tqdm import tqdm
import pickle
import logging
import matplotlib
import matplotlib.ticker as mtick
from gym_dssat_pdi.envs.utils.utils import transpose_dicts
from copy import deepcopy
import matplotlib.gridspec as grid_spec

matplotlib.use('Agg')
from matplotlib.lines import Line2D
from scipy.stats import gaussian_kde
import matplotlib.pyplot as plt
import seaborn as sns
import itertools

import pdb

import sys


class ActionSpace():
    pass


class Batch_Env():
    """
    special batch env, to play policies (1 action == 1 policy)
    """

    def __init__(self, env_dic, batch_generator, bounds_dict, policy, limit_efficiency=None, reward_function=None,
                 reward_feature=None, action_feature=None, alpha=None, delta=None, n_samples=1e4, is_cvar=True,
                 template_maker=None, pickle=True, seed=None, sample_saving_prefix=None, *args, **kwargs):
        self.closed = False
        self.alpha = alpha
        self.delta = delta
        self.pickle = pickle
        self.is_cvar = is_cvar
        self.limit_efficiency = limit_efficiency
        self.bounds_dict = bounds_dict
        self.supp = None
        self.reward_feature = None
        self.set_reward_feature(reward_feature)
        self.eff_supp = bounds_dict['efficiency']
        self.action_values = policy.action_values
        self.n_actions = len(self.action_values)
        self.action_space = ActionSpace()
        self.action_space.n = self.n_actions  # attribute to fit custom bandit framework
        self.policy = policy
        if reward_function is not None:
            self.reward_function = reward_function
        else:
            assert limit_efficiency is not None
        if action_feature is None:
            action_feature = 'cumsumfert'
        if sample_saving_prefix is None:
            sample_saving_prefix = './'
        self.sample_saving_prefix = sample_saving_prefix
        self.action_feature = action_feature
        self.last_interaction = None
        self.env_dic = env_dic
        self.batch_generator = batch_generator
        self.atomic_contexts = batch_generator.atomic_contexts
        self.ingenos = batch_generator.ingenos
        self.n_samples = int(n_samples)
        self.action_samples = None
        self.true_gaps = None
        self.statistics = None
        self.true_params = None
        self.y_kde = None
        self.y_kde_points = None
        self.template_maker = template_maker
        if template_maker is None:
            self.random_inital_nitrogen = False
        else:
            self.random_inital_nitrogen = template_maker.random_inital_nitrogen
        atexit.register(self.close)
        self.seed = seed
        self.set_seed()

    def set_reward_feature(self, reward_feature):
        if reward_feature is None:
            reward_feature = 'reward'
        assert reward_feature in self.bounds_dict and 'efficiency' in self.bounds_dict
        self.reward_feature = reward_feature
        if 'reward' not in self.bounds_dict:
            self.bounds_dict['reward'] = self.bounds_dict[self.reward_feature]
        self.supp = self.bounds_dict[self.reward_feature]

    def reward_function(self, interaction):
        reward = interaction['grnwt'] - interaction['grnwt_0'] - self.limit_efficiency * interaction['cumsumfert']
        return reward

    def step(self, context, ingeno, policy_index, seed=None, with_null_policy=False):
        last_interaction = self.interact_with_atomic_env(context=context,
                                                         ingeno=ingeno,
                                                         policy_index=policy_index,
                                                         seed=seed,
                                                         with_null_policy=with_null_policy)
        return last_interaction

    def interact_with_atomic_env(self, context, ingeno, policy_index, seed=None, verbose=False, with_null_policy=False):
        atomic_env = self.env_dic[context][ingeno]
        if with_null_policy:
            ########## GET NULL POLICY STATS ##########
            null_interactions = []
            atomic_env.reset(seed=seed)  # keep the same seed
            if seed is None:
                seed = atomic_env.seed_value
            action = {'anfer': 0}
            while not atomic_env.done:
                res = atomic_env.step(action)
                new_state, reward, done, info = res
                if new_state is not None:
                    _state = atomic_env._state
                    null_interactions.append(_state)
            ########## /GET NULL POLICY STATS ##########
        ########## GET POLICY STATS ##########
        policy_interactions = []
        atomic_env.reset(seed=seed)
        self.policy.reset()
        while not atomic_env.done:
            observation = atomic_env.observation
            action = self.policy.play_policy(policy_index=policy_index, observation=observation)
            self.last_action = action
            if verbose:
                yrdoy = observation['yrdoy']
                pprint(f'yrdoy : {yrdoy} -> fertilizing {action["anfer"]} kgN/ha')
            res = atomic_env.step(action)
            new_state, reward, done, info = res
            if new_state is not None:
                _state = atomic_env._state
                policy_interactions.append(_state)
        ########## /GET POLICY STATS ##########
        last_interaction = policy_interactions[-1]
        if with_null_policy:
            last_interaction['grnwt_0'] = null_interactions[-1]['grnwt']
        self.last_interaction = last_interaction
        if self.random_inital_nitrogen:
            atomic_env._fileX_template = self.template_maker.make_child_templates(context=context, ingeno=ingeno)
            atomic_env.reset_hard(_new_tmp_folder=True)
        return last_interaction

    def get_true_gaps(self):
        true_gap_dict = {}
        for atomic_context in self.atomic_contexts:
            true_gap_dict[atomic_context] = {}
            for ingeno in [*self.statistics[atomic_context]]:
                cvars = np.asarray(self.statistics[atomic_context][ingeno][self.reward_feature]['cvar'])
                gaps = cvars.max() - cvars
                true_gap_dict[atomic_context][ingeno] = gaps
        self.true_gaps = true_gap_dict

    def set_cluster_restriction(self, contexts_to_keep):
        self.close()
        self.env_dic = {context: self.env_dic[context] for context in contexts_to_keep}
        self.atomic_contexts = contexts_to_keep

    def reset_hard(self, seed=None):
        self.close()
        self.set_seed(seed=seed)
        for atomic_context in self.atomic_contexts:
            for ingeno in [*self.env_dic[atomic_context]]:
                self.env_dic[atomic_context][ingeno].reset_hard(seed=seed)
        self.batch_generator.reset(seed=seed)
        self.policy.reset()
        self.closed = False

    def reset(self, seed=None):
        self.set_seed(seed=seed)
        for atomic_context in self.atomic_contexts:
            for ingeno in [*self.env_dic[atomic_context]]:
                self.env_dic[atomic_context][ingeno].reset(seed=seed)
        self.policy.reset()
        self.batch_generator.reset(seed=seed)
        self.closed = False

    def close(self):
        for atomic_context in self.atomic_contexts:
            for ingeno in [*self.env_dic[atomic_context]]:
                self.env_dic[atomic_context][ingeno].close()
        self.closed = True

    def set_seed(self, seed=None):
        if seed is None:
            seed = self.seed
        else:
            self.seed = seed
        np.random.seed(seed)
        if self.template_maker is not None:
            self.template_maker.set_seed(seed=seed)
        self.batch_generator.set_seed(seed=seed)
        for atomic_context in self.atomic_contexts:
            for ingeno in self.ingenos:
                self.env_dic[atomic_context][ingeno].set_seed(seed=seed)

    def get_dist_params(self, compute_samples, saving_prefix=None, n_samples=None, compute_kdes=False):
        n_cores = mp.cpu_count()
        n_contexts = len(self.atomic_contexts)
        n_ingenos = len(self.ingenos) + 1
        if saving_prefix is None:
            saving_prefix = self.sample_saving_prefix
        else:
            self.sample_saving_prefix = saving_prefix
        if compute_samples:
            if not n_samples:
                n_samples = self.n_samples
            self.n_samples = n_samples
            samples_per_core = n_samples // n_cores
            print('\n~~~ sampling stage starts ~~~')
            self.close()
            for context_index, atomic_context in enumerate(self.atomic_contexts):
                print(f'~~~ {atomic_context} context: {context_index + 1}/{n_contexts} ~~~')
                for ingeno_index, ingeno in tqdm(enumerate(self.ingenos), position=1, colour='red',
                                                 desc=f'context {atomic_context}'):
                    context_ingeno_samples = {}
                    print(f'~~~ {ingeno} ingeno: {ingeno_index + 1}/{n_ingenos} ~~~')
                    seeds = np.random.choice(range(1000000), size=samples_per_core * n_cores, replace=False).reshape(
                        (n_cores, samples_per_core))
                    action_indexes = [-1, *range(self.n_actions)]
                    for policy_index in tqdm(action_indexes, position=2, colour='green',
                                             desc=f'atomic_context {atomic_context} ; ingeno {ingeno}'):
                        arguments = [(self, seeds[core], atomic_context, ingeno, policy_index, samples_per_core)
                                     for core in range(n_cores)]
                        last_interactions = []
                        with mp.Pool(processes=n_cores) as pool:
                            # Warning: order needed to compare to null policy!
                            for results_element in tqdm(pool.imap(parallel_sampling_, arguments),
                                                        total=len(arguments),
                                                        position=0,
                                                        leave=False):
                                last_interactions.append(results_element)
                        last_interactions = transpose_dicts(np.concatenate(last_interactions, axis=None))
                        context_ingeno_samples[policy_index] = last_interactions
                    to_save = {'samples': context_ingeno_samples,
                               'n_samples': self.n_samples,
                               'ingeno': ingeno,
                               'atomic_context': atomic_context}
                    saving_path = f'{saving_prefix}samples_c_{atomic_context}_i_{ingeno}_n_{self.n_samples}.pkl'
                    if self.pickle:
                        with open(saving_path, 'wb') as f:
                            pickle.dump(to_save, f, protocol=pickle.HIGHEST_PROTOCOL)
                    else:
                        dic_to_json(to_save, saving_path=saving_path)
                    del to_save
            print('~~~ sampling stage completed ~~~')

        print('~~~ statistics computation starts ~~~')
        self.statistics = {}
        self.true_params = {}
        for context_index, atomic_context in enumerate(self.atomic_contexts):
            print(f'~~~ context {atomic_context}: {context_index + 1}/{n_contexts} ~~~')
            all_cultivars_samples = {}
            for ingeno_index, ingeno in enumerate([*self.ingenos, 'all']):
                print(f'~~~ ingeno {ingeno} : {ingeno_index + 1}/{n_ingenos} ~~~')
                if ingeno != 'all':
                    sample_loading_path = f'{saving_prefix}samples_c_{atomic_context}_i_{ingeno}_n_{self.n_samples}.pkl'
                    sample_dics = self.load_samples(loading_path=sample_loading_path)
                    sample_dics = self.get_sample_efficiency(sample_dics=sample_dics)
                    filter = True
                else:
                    sample_dics = all_cultivars_samples
                    filter = False
                features = ['reward', 'cumsumfert', 'efficiency', 'grnwt']
                for feature in features:
                    samples = self.compute_statistic(sample_dics=sample_dics,
                                                     feature=feature,
                                                     atomic_context=atomic_context,
                                                     ingeno=ingeno,
                                                     filter=filter)
                    if ingeno != 'all':
                        for policy_index in range(self.n_actions):
                            if policy_index not in all_cultivars_samples:
                                all_cultivars_samples[policy_index] = {}
                            if feature not in all_cultivars_samples[policy_index]:
                                all_cultivars_samples[policy_index][feature] = []
                            all_cultivars_samples[policy_index][feature].extend(samples[policy_index])
                if self.is_cvar:
                    true_param_key = 'cvar'
                else:
                    true_param_key = 'mean'
                if atomic_context not in self.true_params:
                    self.true_params[atomic_context] = {}
                self.true_params[atomic_context][ingeno] = self.statistics[atomic_context][ingeno]['reward'][
                    true_param_key]
            if compute_kdes:
                self.compute_kdes()
        self.get_true_gaps()

    def compute_statistic(self, sample_dics, feature, atomic_context, ingeno, filter=True):
        if atomic_context not in self.statistics:
            self.statistics[atomic_context] = {}
        if ingeno not in self.statistics[atomic_context]:
            self.statistics[atomic_context][ingeno] = {}
        self.statistics[atomic_context][ingeno][feature] = {'mean': [], 'std': [], 'cvar': []}
        if filter:
            samples = self.filter_samples(sample_dics=sample_dics, feature=feature)
        else:
            samples = [sample_dics[policy_index][feature] for policy_index in range(self.n_actions)]
        for policy_index in range(self.n_actions):
            sample = samples[policy_index]
            cvar = moment_empirical_cvar(sample, self.alpha)
            self.statistics[atomic_context][ingeno][feature]['cvar'].append(cvar)
            sample = np.ma.masked_invalid(sample)
            mean = sample.mean()
            std = sample.std(ddof=1)
            self.statistics[atomic_context][ingeno][feature]['mean'].append(mean)
            self.statistics[atomic_context][ingeno][feature]['std'].append(std)
        return samples

    def filter_samples(self, sample_dics, feature):
        if feature == 'reward':
            result = self.get_reward_from_samples(sample_dics)
        else:
            result = {action_index: sample_dics[action_index][feature] for action_index in range(self.n_actions)}
        return result

    def get_reward_from_samples(self, sample_dics):
        result_dic = {}
        all_grwnt_0 = sample_dics[-1]['grnwt']
        for action_index in range(self.n_actions):
            interactions = sample_dics[action_index]
            interactions['grnwt_0'] = all_grwnt_0
            list_of_dicts = [dict(zip(interactions, t)) for t in zip(*interactions.values())]
            rewards = [self.reward_function(dic) for dic in list_of_dicts]
            result_dic[action_index] = rewards
        return result_dic

    def get_sample_efficiency(self, sample_dics):
        all_grwnt_0 = sample_dics[-1]['grnwt']
        for action_index in range(self.n_actions):
            all_grwnt = sample_dics[action_index]['grnwt']
            all_cumsumfert = sample_dics[action_index]['cumsumfert']
            efficiencies = []
            for grwnt_0, grnwt, cumsumfert in zip(all_grwnt_0, all_grwnt, all_cumsumfert):
                efficiency = self.get_efficiency(grnwt_0=grwnt_0, grnwt=grnwt, cumsumfert=cumsumfert)
                efficiencies.append(efficiency)
            sample_dics[action_index]['efficiency'] = efficiencies
            sample_dics[action_index]['grnwt_0'] = all_grwnt_0
        return sample_dics

    @staticmethod
    def get_efficiency(grnwt, grnwt_0, cumsumfert):
        if cumsumfert == 0:
            efficiency = np.nan
        else:
            efficiency = (grnwt - grnwt_0) / cumsumfert
        return efficiency

    def print_action_statistics(self):
        print('\n~~~~~ environment statistics ~~~~~')
        pprint(self.statistics)

    def load_samples(self, loading_path):
        if self.pickle:
            with open(loading_path, 'rb') as f:
                saved_sampling = pickle.load(f)
        else:
            saved_sampling = load_json(loading_path)
        samples = saved_sampling['samples']
        return samples

    def compute_kdes(self, plot_feature=None):
        if plot_feature is None:
            plot_feature = self.reward_feature
        self.y_kde_points = {}
        self.y_kde = {}
        n_contexts = len(self.atomic_contexts)
        n_ingenos = len(self.ingenos) + 1
        print('~~~ \sampled loading for kdes ~~~\n')
        for index_context, atomic_context in enumerate(self.atomic_contexts):
            self.y_kde_points[atomic_context] = {}
            self.y_kde[atomic_context] = {}
            print(f'~~~ {atomic_context} context: {index_context + 1}/{n_contexts} ~~~')
            all_cultivar_values = [[] for _ in range(self.n_actions)]
            for index_ingeno, ingeno in enumerate([*self.ingenos, 'all']):
                print(f'~~~ ingeno {ingeno} : {index_ingeno + 1}/{n_ingenos} ~~~')
                if ingeno != 'all':
                    samples_loading_path = f'{self.sample_saving_prefix}samples_c_{atomic_context}_i_{ingeno}_n_{self.n_samples}.pkl'
                    samples = self.load_samples(loading_path=samples_loading_path)
                    if plot_feature == 'reward':
                        values_to_plot = self.get_reward_from_samples(samples)
                    else:
                        if plot_feature == 'efficiency':
                            samples = self.get_sample_efficiency(sample_dics=samples)
                        values_to_plot = self.filter_samples(sample_dics=samples, feature=plot_feature)
                else:
                    values_to_plot = all_cultivar_values
                self.y_kde_points[atomic_context][ingeno] = []
                self.y_kde[atomic_context][ingeno] = []
                for policy_index in range(self.n_actions):
                    print(f'~~ computing kde for action #{policy_index} ~~')
                    sample = values_to_plot[policy_index]
                    sample = np.asarray(sample)
                    sample = sample[~np.isnan(sample)]
                    # sample = np.nan_to_num(sample, nan=0)
                    if ingeno != 'all':
                        all_cultivar_values[policy_index].extend(sample)
                    x_kde = np.linspace(start=self.supp[0], stop=self.supp[1], num=10000)
                    kde_f = gaussian_kde(sample)
                    y_kde = kde_f(x_kde)
                    x_kde_points_step = self.supp[1] / 10
                    x_kde_points = np.arange(start=x_kde_points_step, stop=self.supp[1], step=x_kde_points_step)
                    y_kde_points = kde_f(x_kde_points)
                    self.y_kde_points[atomic_context][ingeno].append(y_kde_points)
                    self.y_kde[atomic_context][ingeno].append(y_kde)

    def render_env(self, saving_prefix='./', saving_suffix_=None, title_=None, cvar_bars=True, mean_bars=True,
                   del_kde=True, reward_feature_layout=None):
        print('\n#### ENV RENDERING STAGE ###')
        if reward_feature_layout is not None:
            assert self.reward_feature in reward_feature_layout
        n_contexts = len(self.atomic_contexts)
        n_ingenos = len(self.ingenos) + 1
        if self.y_kde is None:
            self.compute_kdes()
        widths = np.linspace(start=1, stop=5, num=self.n_actions)
        widths = widths[::-1]
        for index_context, atomic_context in enumerate(self.atomic_contexts):
            print(f'~~~ rendering {atomic_context} context: {index_context + 1}/{n_contexts} ~~~')
            for index_ingeno, ingeno in enumerate([*self.ingenos, 'all']):
                fig, ax = plt.subplots()
                legend_elements = []
                palette = itertools.cycle(sns.color_palette('bright'))
                markers = itertools.cycle(['o', '^', 's', 'p', 'X', 'D', 'd'])
                print(f'~~~ ingeno {ingeno}: {index_ingeno + 1}/{n_ingenos} ~~~')
                for policy_index, policy_name in zip(range(self.n_actions), self.action_values):
                    cvars = self.statistics[atomic_context][ingeno][self.reward_feature]['cvar'][policy_index]
                    means = self.statistics[atomic_context][ingeno][self.reward_feature]['mean'][policy_index]
                    label = policy_name
                    marker = next(markers)
                    dash = (1, 0, 0, 0)
                    color = next(palette)
                    x_kde = np.linspace(start=self.supp[0], stop=self.supp[1], num=10000)
                    y_kde = self.y_kde[atomic_context][ingeno][policy_index]
                    y_kde_points = self.y_kde_points[atomic_context][ingeno][policy_index]
                    kde_p = ax.plot(x_kde, y_kde, alpha=1, color=color, zorder=1, dashes=dash,
                                    linewidth=widths[policy_index])
                    x_kde_points_step = self.supp[1] / 10
                    x_kde_points = np.arange(start=x_kde_points_step, stop=self.supp[1], step=x_kde_points_step)
                    points_kde = ax.plot(x_kde_points, y_kde_points, color=color, marker=marker,
                                         zorder=3, markersize=8, linestyle='')
                    legend_element = Line2D([None], [None], label=label, dashes=dash, marker=marker,
                                            color=color, markersize=8, linewidth=3)
                    legend_elements.append(legend_element)
                    if cvar_bars:
                        ax.axvline(x=cvars, color=color, linestyle='--', linewidth=2, zorder=0)
                    if mean_bars:
                        ax.axvline(x=means, color=color, linestyle='--', linewidth=2)
                upper_bound = ax.axvline(x=self.supp[1], color='b', linestyle='dashdot', linewidth=2, zorder=0,
                                         label=f'{self.reward_feature} u. b.')
                if cvar_bars:
                    metric_legend_label = f'CVaR @{100 * self.alpha:.0f}%'
                    metric_legend = Line2D([None], [None], color='black', lw=2, label=metric_legend_label,
                                           linestyle='--')
                    legend_elements.append(metric_legend)
                if mean_bars:
                    metric_legend_label = f'mean'
                    metric_legend = Line2D([None], [None], color='black', lw=2, label=metric_legend_label,
                                           linestyle='--')
                    legend_elements.append(metric_legend)
                legend_elements.append(upper_bound)
                if reward_feature_layout is not None:
                    ax.set_xlabel(reward_feature_layout[self.reward_feature])
                ax.set_ylabel('density')
                ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.1e'))
                ax.set_aspect('auto')
                ax.set_xlim([self.supp[0], 1.05 * self.supp[1]])
                ax.set_ylim(bottom=0)
                ax.set_xbound(lower=self.supp[0], upper=1.05 * self.supp[1])
                if title_ is None:
                    title = f'empirical distributions of {self.reward_feature}' \
                            f'\n for soil {atomic_context} and cultivar {ingeno}'
                    if self.reward_feature == 'reward':
                        title += f'\n NAE_lim: {self.limit_efficiency} kg grain/kg N'
                    title += f'\n (#{self.n_samples:.0e} samples)'

                else:
                    title = title_
                plt.title(title)
                # ax.legend(handles=legend_elements, loc=1)
                ax.legend(handles=legend_elements, bbox_to_anchor=(1.04, 1), borderaxespad=0)
                plt.tight_layout()
                if saving_suffix_ is None:
                    if self.reward_feature != 'reward':
                        last_descriptor = self.reward_feature
                    else:
                        last_descriptor = f'{self.reward_feature}_NAElim_{self.limit_efficiency}'
                    saving_suffix = f'dssat_render_c_{atomic_context}_i_{ingeno}_n_{self.n_samples}_a' \
                                    f'_{100 * self.alpha:.0f}p_r_{last_descriptor}.pdf'
                else:
                    saving_suffix = saving_suffix_
                saving_path = f'{saving_prefix}{saving_suffix}'
                plt.savefig(saving_path)
                plt.close()
        if del_kde:
            del self.y_kde
            del self.y_kde_points
            self.y_kde = None
            self.y_kde_points = None

    def render_env_ridge_plot(self, saving_prefix='./', saving_suffix_=None, title_=None, cvar_bars=True, mean_bars=True,
                    del_kde=True, reward_feature_layout=None):
        print('\n#### ENV RENDERING STAGE ###')
        if reward_feature_layout is not None:
            assert self.reward_feature in reward_feature_layout
        n_contexts = len(self.atomic_contexts)
        n_ingenos = len(self.ingenos) + 1
        if self.y_kde is None:
            self.compute_kdes()
        widths = np.linspace(start=1, stop=5, num=self.n_actions)
        widths = widths[::-1]
        for index_context, atomic_context in enumerate(self.atomic_contexts):
            print(f'~~~ rendering {atomic_context} context: {index_context + 1}/{n_contexts} ~~~')
            for index_ingeno, ingeno in enumerate([*self.ingenos, 'all']):
                if title_ is None:
                    if self.reward_feature == 'reward':
                        feature_name = 'Yield excess'
                    else:
                        feature_name = self.reward_feature
                    title = f'{feature_name} empirical distributions'
                else:
                    title = title_
                fig = plt.figure(figsize=(6.4 * .75, 4.8))
                palette = itertools.cycle(sns.color_palette('bright', n_colors=self.n_actions))
                palette2 = itertools.cycle(sns.color_palette('bright', n_colors=self.n_actions))
                palette3 = itertools.cycle(sns.color_palette('bright', n_colors=self.n_actions))
                print(f'~~~ ingeno {ingeno}: {index_ingeno + 1}/{n_ingenos} ~~~')
                gs = grid_spec.GridSpec(self.n_actions, 1)
                all_y_kdes = [self.y_kde[atomic_context][ingeno][policy_index] for policy_index in
                              range(self.n_actions)]
                max_y_kde = np.concatenate(all_y_kdes, axis=None).max()
                axs = []
                policy_indexes = range(self.n_actions)
                policy_names = self.action_values
                for policy_index, policy_name in zip(policy_indexes, policy_names):
                    color = next(palette)
                    color2 = next(palette2)
                    reversed_index = self.n_actions - 1 - policy_index  # start plotting from bottom to top
                    ax = fig.add_subplot(gs[reversed_index:reversed_index + 1, 0:])
                    axs.append(ax)
                    fig.text(-0.04, 0.01, policy_name, fontsize=10, horizontalalignment='left',
                             verticalalignment='bottom', transform=ax.transAxes)
                    x_kde = np.linspace(start=self.supp[0], stop=self.supp[1], num=10000)
                    y_kde = self.y_kde[atomic_context][ingeno][policy_index]
                    ax.plot(x_kde, y_kde, alpha=1, color=color2, linewidth=2)
                    ax.fill_between(x_kde, y_kde, alpha=.1, color=color)
                    # if policy_index == 0:
                    #     x_pos = (self.supp[0] + self.supp[1]) / 2
                    #     y_pos = -.1
                    #     ax.text(x_pos, y_pos, title, fontsize=10, horizontalalignment='center',
                    #              verticalalignment='bottom')
                    ax.set_xlim([self.supp[0], 1.05 * self.supp[1]])
                    ax.set_ylim(bottom=0, top=max_y_kde)
                    ax.set_xbound(lower=self.supp[0], upper=1.05 * self.supp[1])
                    ax.set_yticklabels([])
                    ax.tick_params(
                        axis='y',
                        which='both',  # both major and minor ticks are affected
                        bottom=False,
                        top=False,
                        left=False,
                        right=False,
                        labelbottom=False,
                        labeltop=False,
                        labelleft=False,
                        labelright=False)
                    if policy_index > 0:
                        ax.set_xticklabels([])
                        ax.tick_params(
                            axis='x',
                            which='both',  # both major and minor ticks are affected
                            bottom=False,
                            top=False,
                            left=False,
                            right=False,
                            labelbottom=False,
                            labeltop=False,
                            labelleft=False,
                            labelright=False)
                    else:
                        ax.set_xlabel(reward_feature_layout[self.reward_feature])
                    spines = ["top", "right", "left", "bottom"]
                    for s in spines:
                        ax.spines[s].set_visible(False)

                    rect = ax.patch
                    rect.set_alpha(0)
                if cvar_bars:
                    metrics = self.statistics[atomic_context][ingeno][self.reward_feature]['cvar']
                else:
                    metrics = self.statistics[atomic_context][ingeno][self.reward_feature]['mean']
                for ax, metric in zip(axs, metrics):
                    color3 = next(palette3)
                    ax.plot(metric, 0, marker='*', zorder=100, color=color3, clip_on=False, markersize=8)
                gs.update(hspace=-0.8)
                # upper_bound = ax.axvline(x=self.supp[1], color='b', linestyle='dashdot', linewidth=2, zorder=0,
                #                          label=f'{self.reward_feature} u. b.')
                # plt.tight_layout()
                if saving_suffix_ is None:
                    if self.reward_feature != 'reward':
                        last_descriptor = self.reward_feature
                    else:
                        last_descriptor = f'{self.reward_feature}_NAElim_{self.limit_efficiency}'
                    saving_suffix = f'dssat_render_c_{atomic_context}_i_{ingeno}_n_{self.n_samples}_a' \
                                    f'_{100 * self.alpha:.0f}p_r_{last_descriptor}.pdf'
                else:
                    saving_suffix = saving_suffix_
                saving_path = f'{saving_prefix}{saving_suffix}'
                plt.savefig(saving_path)
                plt.close()
        if del_kde:
            del self.y_kde
            del self.y_kde_points
            self.y_kde = None
            self.y_kde_points = None


def parallel_sampling_(args):
    try:
        batch_env, seeds, atomic_context, ingeno, policy_index, samples_per_core = args
        batch_env.env_dic[atomic_context][ingeno].reset_hard(_new_tmp_folder=True)
        seeds = itertools.cycle(seeds)
        last_interactions = []
        for _ in range(samples_per_core):
            seed = next(seeds)
            last_interaction = batch_env.interact_with_atomic_env(context=atomic_context,
                                                                  ingeno=ingeno,
                                                                  policy_index=policy_index,
                                                                  seed=seed)
            last_interactions.append(last_interaction)
        return last_interactions
    except Exception as e:
        logging.exception(e)
    finally:
        batch_env.env_dic[atomic_context][ingeno].close()


def fill_default_action_value_dict(default_action_dict, batch_env, batch_generator):
    cvar_dict = {}
    for cluster in [*default_action_dict]:
        default_action = default_action_dict[cluster]
        atomic_contexts = batch_generator.cluster_to_context_mapping[cluster]
        values = []
        for atomic_context in atomic_contexts:
            values.append(batch_env.cvars[atomic_context][default_action])
        cvar_dict[cluster] = {default_action: np.mean(values)}
    return cvar_dict
