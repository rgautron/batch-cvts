from cvarBandits.bandits.bandit_cvar import B_CVTS
from cvarBandits.utils.estimators import moment_empirical_cvar
import batch_utils
from copy import deepcopy
import itertools
import numpy as np
import tqdm
import pdb


class B_CVTS_BATCH(B_CVTS):
    def __init__(self, batch_env, atomic_contexts, cluster, feature_threshold=None, alpha=None, delta=None,
                 init_pulls=1, limits=None, horizon=None, seed=None, n_batch_window=None,
                 default_action_values_dict=None, initial_batch_action_probability=None, *args, **kwargs):
        self.atomic_contexts = atomic_contexts
        self.cluster = cluster
        self.feature_threshold = feature_threshold  # None if no thresholding
        self.init_bonus = True
        self.T = 0
        self.step_seeds = None
        self.horizon = horizon
        self.mean_noisy_efficiencies = [[] for _ in range(batch_env.n_actions)]
        self.efficiencies = [[] for _ in range(batch_env.n_actions)]
        self.efficiencies_ = [[] for _ in range(batch_env.n_actions)]
        if feature_threshold is not None:
            self.eligible_action_indexes = []
        else:
            self.eligible_action_indexes = range(batch_env.n_actions)
        self.context_history = []
        self.reward_history = []
        self.efficiency_history = []
        self.n_batch_window = n_batch_window
        self.sliding_count_dict = []
        if default_action_values_dict is None:
            default_action_values_dict = {cluster: []}
        self.default_action_values_dict = default_action_values_dict[cluster]
        self.default_action_indexes = []
        if bool(default_action_values_dict):  # non empty dict
            assert cluster in [*default_action_values_dict]
            self.default_action_indexes = [*default_action_values_dict[cluster]]
        else:
            self.default_action_indexes = []
        self.available_actions = [action_index for action_index in range(batch_env.n_actions)
                                  if action_index not in self.default_action_indexes]
        if initial_batch_action_probability is None:
            n_actions = batch_env.n_actions
            probability = 1 / (
                    batch_env.n_actions - len(self.default_action_indexes))  # equiprob. unknown actions
            initial_batch_action_probability = [
                probability if action_index not in self.default_action_indexes else 0 for action_index in
                range(n_actions)]
        else:
            assert sum(initial_batch_action_probability) == 1 and len(
                initial_batch_action_probability) == batch_env.n_actions
        self.initial_batch_action_probability = initial_batch_action_probability
        if alpha is None:
            alpha = batch_env.alpha
        assert 0 < alpha <= 1
        super().__init__(batch_env, delta=delta, alpha=alpha, limits=limits, init_pulls=init_pulls, horizon=horizon,
                         seed=seed, init=False)
        self.set_seed()

    def get_criteria(self, action_index):
        rewards = self.rewards_[action_index]
        if self.n_batch_window is not None and len(
                self.action_history) > self.n_batch_window:  # Python allows short
            # circuiting
            if action_index in self.sliding_count_dict:
                rewards = [rewards[0], *rewards[:self.sliding_count_dict[action_index]]]
            else:
                rewards = rewards[:1]
        weights = np.random.dirichlet(np.ones(len(rewards)))
        if self.alpha < 1:
            samples = np.sort(rewards)
            q_index = int(np.sum(weights.cumsum() <= self.alpha))
            q_value = samples[q_index]
            criteria = q_value - 1 / self.alpha * (weights * np.maximum(q_value - samples, 0)).sum()
        else:
            criteria = np.dot(rewards, weights)
        self.criteria[action_index] = criteria
        if self.feature_threshold is not None:
            self.update_noisy_efficiencies(action_index=action_index, weights=weights)
        return criteria

    def update_criteria(self):
        if self.n_batch_window is not None and len(self.action_history) > self.n_batch_window:
            self.get_sliding_count_dict()
        for action_index in range(self.n_actions):
            if action_index not in self.default_action_indexes:
                self.get_criteria(action_index=action_index)
        if self.feature_threshold is not None:
            self.get_eligible_actions()

    def step(self, contexts, ingenos, seeds=None):
        action_history_list = []
        reward_history_list = []
        efficiency_history_list = []
        for _ in contexts:
            self.update_criteria()  # resample the Dirichlet
            action_index = self.choice_function()
            action_history_list.append(action_index)
        for action_index, context, ingeno in zip(action_history_list, contexts, ingenos):
            self.update_criteria()
            if seeds is None:
                seed = None
            else:
                seed = next(seeds)
            interaction = self.env.step(context=context,
                                        ingeno=ingeno,
                                        policy_index=action_index,
                                        seed=seed,
                                        with_null_policy=True)
            reward = self.env.reward_function(interaction)
            efficiency = self.env.get_efficiency(grnwt=interaction['grnwt'],
                                                 grnwt_0=interaction['grnwt_0'],
                                                 cumsumfert=interaction['cumsumfert'])
            reward_history_list.append(reward)
            efficiency_history_list.append(efficiency)
            self.rewards[action_index].append(reward)
            self.efficiencies[action_index].append(efficiency)
            if action_index not in self.default_action_indexes:
                self.rewards_[action_index].append(reward)
                self.efficiencies_[action_index].append(efficiency)
            self.pulls[action_index] += 1
            self.t += 1
        self.T += 1
        self.reward_history.append(reward_history_list)
        self.action_history.append(action_history_list)
        self.efficiency_history.append(efficiency_history_list)
        self.context_history.append(contexts)
        return action_history_list, reward_history_list, efficiency_history_list, contexts

    def choice_function(self):
        eligible_action_indexes = np.asarray(self.eligible_action_indexes)
        criteria = np.asarray(self.criteria)[eligible_action_indexes]  # subset for eligible actions
        all_candidates = np.argwhere(criteria == self.choice_criteria(criteria)).flatten()  # looking for multiple
        # min/max
        all_candidates = eligible_action_indexes[all_candidates]  # go back to original indexes
        if len(all_candidates) == 1:
            selected, = all_candidates
        else:
            pulls = self.pulls[all_candidates]
            selected = all_candidates[np.argmin(pulls)]  # !! returns first min by default
        return selected

    def get_eligible_actions(self):
        eligible_action_indexes = np.argwhere(
            np.asarray(self.mean_noisy_efficiencies) >= self.feature_threshold).flatten().tolist()
        eligible_action_indexes.extend(
            np.argwhere(np.isnan(self.mean_noisy_efficiencies)).flatten().tolist())  # if mean noisy
        # efficiency is np.nan, then it is also candidate
        if len(eligible_action_indexes) == 0:  # if no eligible actions, all actions are candidate
            eligible_action_indexes = range(self.n_actions)
        self.eligible_action_indexes = eligible_action_indexes

    # TODO: add default efficiency as the default cvar for default action

    def update_noisy_efficiencies(self, action_index, weights=None):
        # TODO: should add sliding window
        efficiencies = self.efficiencies_[action_index]
        if len(efficiencies) == 0:
            mean_efficiency = np.nan
        else:
            if weights is None:
                n = len(efficiencies)
                weights = np.repeat(1 / n, repeats=n)
            else:
                assert len(efficiencies) == len(weights)
            noisy_efficiencies = efficiencies * weights
            noisy_efficiencies_masked = np.ma.masked_invalid(noisy_efficiencies)
            if noisy_efficiencies_masked.mask.all():  # all values are np.nan
                mean_efficiency = np.nan
            else:
                mean_efficiency = np.mean(noisy_efficiencies_masked)
        self.mean_noisy_efficiencies[action_index] = mean_efficiency

    def initialize(self, farmer_init_dict, n_init_context_batch=None, seeds=None):
        farmers_init_list = self.env.batch_generator.farmer_sampling_bandit_init(cluster=self.cluster,
                                                                                 farmers_init_dict=farmer_init_dict,
                                                                                 available_action_index=self.available_actions,
                                                                                 n_init_context_batch=n_init_context_batch)
        contexts_init = []
        reward_history_list = []
        efficiency_history_list = []
        action_index_list = []
        if self.default_action_indexes:
            for default_action_index in self.default_action_indexes:
                self.criteria[default_action_index] = self.default_action_values_dict[default_action_index]
        if self.init_bonus:
            for action_index in [action_index for action_index in range(self.n_actions) if
                                 action_index not in self.default_action_indexes]:  # set bonus for all others
                self.rewards_[action_index].append(self.env.supp[-1])
                self.efficiencies_[action_index].append(self.env.eff_supp[-1])
        if len(farmers_init_list) == self.n_actions:
            action_indexes = range(self.n_actions)
        else:
            action_indexes = []
            while not np.all(np.isin(self.available_actions, action_indexes)):
                action_indexes = np.random.choice(self.available_actions, p=self.initial_batch_action_probability,
                                                  size=len(farmers_init_list))
        # In case of a default arm, the probability of sampling for the default arm is set to 0
        for farmer, action_index in zip(farmers_init_list, action_indexes):
            context = farmer.context
            ingeno = farmer.ingeno
            contexts_init.append(context)
            default_action_indexes = self.default_action_indexes
            action_index_list.append(action_index)
            if action_index not in default_action_indexes:
                if seeds is None:
                    seed = None
                else:
                    seed = next(seeds)
                interaction = self.env.step(context=context,
                                            ingeno=ingeno,
                                            policy_index=action_index,
                                            seed=seed,
                                            with_null_policy=True)
                reward = self.env.reward_function(interaction)
                efficiency = self.env.get_efficiency(grnwt=interaction['grnwt'],
                                                     grnwt_0=interaction['grnwt_0'],
                                                     cumsumfert=interaction['cumsumfert'])
                reward_history_list.append(reward)
                efficiency_history_list.append(efficiency)
                self.rewards_[action_index].append(reward)
                self.rewards[action_index].append(reward)
                self.efficiencies_[action_index].append(efficiency)
                self.efficiencies[action_index].append(efficiency)
                self.pulls[action_index] += 1
                self.t += 1
        self.T += 1
        self.action_history.append(action_index_list)
        self.context_history.append(contexts_init)
        self.reward_history.append(reward_history_list)
        self.efficiency_history.append(efficiency_history_list)
        return action_index_list, reward_history_list, efficiency_history_list, contexts_init, farmers_init_list

    def get_sliding_count_dict(self):
        sliding_action_history = self.action_history[
                                 -self.n_batch_window:]  # we ensured before: len(self.action_history) > self.n_batch_window
        sliding_action_history = np.concatenate(sliding_action_history, axis=None).tolist()
        self.sliding_count_dict = batch_utils.utils.list_to_count_dic(sliding_action_history)
        # print(self.sliding_count_dict)

    def reset(self, seed=None):
        if seed is None:
            seed = self.seed
        else:
            self.seed = seed
        self.set_seed(seed=seed)
        self.rewards = [[] for _ in range(self.n_actions)]
        self.pulls = np.zeros(self.n_actions)
        self.vars_ = np.repeat(np.nan, self.n_actions)
        self.means = np.repeat(np.nan, self.n_actions)
        self.criteria = np.repeat(np.nan, self.n_actions)
        self.t = 0
        self.frames = []
        self.context = None
        self.action_history = []
        self.regrets = []
        self.extra_reset()

    def extra_reset(self):
        self.T = 0
        self.rewards_ = [[] for _ in range(self.n_actions)]
        self.context_history = [[] for _ in range(self.n_actions)]
        self.action_history = [[] for _ in range(self.n_actions)]
        self.reward_history = [[] for _ in range(self.n_actions)]

    def reset_hard(self, seed=None):
        self.env.reset_hard(seed=seed)
        self.reset(seed=seed)

    def close(self):
        self.env.close()

    def pull_once(self):
        pass

    def get_means(self, action_index):
        pass

    def get_vars(self, action_index):
        pass


class ETC_CVAR_BATCH(B_CVTS_BATCH):
    def __init__(self, batch_env, atomic_contexts, n_batch_explore, cluster, feature_threshold=None, alpha=None,
                 delta=None, init_pulls=1, limits=None, horizon=None, seed=None, n_batch_window=None,
                 default_action_values_dict=None, n_init_context_batch=None, initial_batch_action_probability=None,
                 *args, **kwargs):
        super().__init__(batch_env=batch_env, atomic_contexts=atomic_contexts, cluster=cluster,
                         feature_threshold=feature_threshold, alpha=alpha, delta=delta, init_pulls=init_pulls,
                         limits=limits, horizon=horizon, seed=seed, n_batch_window=n_batch_window,
                         default_action_values_dict=default_action_values_dict,
                         n_init_context_batch=n_init_context_batch,
                         initial_batch_action_probability=initial_batch_action_probability)
        self.n_batch_explore = n_batch_explore
        self.init_bonus = False

    def step(self, contexts, ingenos, seeds=None):
        if self.T < self.n_batch_explore:
            if len(contexts) >= len(self.available_actions):
                if len(contexts) == len(self.available_actions):  # avoid long resampling
                    action_indexes = self.available_actions
                else:
                    samples_per_action = len(contexts) // self.n_actions
                    remainder = len(contexts) % self.n_actions
                    sample_numbers = [samples_per_action for _ in range(self.n_actions)]
                    random_action_indexes = np.random.choice(self.n_actions, size=remainder, replace=False)
                    for random_action_index in random_action_indexes:
                        sample_numbers[random_action_index] += 1
                    action_indexes = []
                    for action_index, sample_number in enumerate(sample_numbers):
                        action_indexes.extend([action_index for _ in range(sample_number)])
                    # while not np.all(np.isin(self.available_actions, action_indexes)):  # sample all available actions
                    #     action_indexes = np.random.choice(self.available_actions, size=len(contexts), replace=True)
            else:
                action_indexes = np.random.choice(self.n_actions, size=len(contexts), replace=False)  # not enough
                # actions so randomly distribute trials
        elif self.T == self.n_batch_explore:
            self.update_criteria()
        action_history_list = []
        reward_history_list = []
        efficiency_history_list = []
        for index, (context, ingeno) in enumerate(zip(contexts, ingenos)):
            if self.T < self.n_batch_explore:
                action_index = action_indexes[index]
            else:
                action_index = self.choice_function()
            if seeds is None:
                seed = None
            else:
                seed = next(seeds)
            interaction = self.env.step(context=context,
                                        ingeno=ingeno,
                                        policy_index=action_index,
                                        seed=seed,
                                        with_null_policy=True)
            reward = self.env.reward_function(interaction)
            efficiency = self.env.get_efficiency(grnwt=interaction['grnwt'],
                                                 grnwt_0=interaction['grnwt_0'],
                                                 cumsumfert=interaction['cumsumfert'])
            reward_history_list.append(reward)
            action_history_list.append(action_index)
            efficiency_history_list.append(efficiency)
            # TODO: cleaning
            self.rewards[action_index].append(reward)
            if action_index not in self.default_action_indexes:
                self.rewards_[action_index].append(reward)
            self.pulls[action_index] += 1
            self.t += 1
        self.T += 1
        self.reward_history.append(reward_history_list)
        self.action_history.append(action_history_list)
        self.efficiency_history.append(efficiency_history_list)
        self.context_history.append(contexts)
        return action_history_list, reward_history_list, efficiency_history_list, contexts

    def update_criteria(self):
        if self.T == self.n_batch_explore:
            if self.n_batch_window is not None and len(self.action_history) > self.n_batch_window:
                self.get_sliding_count_dict()
            for action_index in range(self.n_actions):
                if action_index not in self.default_action_indexes:
                    self.get_criteria(action_index=action_index)
            if self.feature_threshold is not None:
                self.get_eligible_actions()
        else:
            pass

    def get_criteria(self, action_index):
        rewards = self.rewards_[action_index]
        if self.n_batch_window is not None and len(
                self.action_history) > self.n_batch_window:  # Python allows short
            # circuiting
            if action_index in self.sliding_count_dict:
                rewards = [rewards[0], *rewards[:self.sliding_count_dict[action_index]]]
            else:
                rewards = rewards[:1]
        criteria = moment_empirical_cvar(samples=rewards, alpha=self.alpha)
        self.criteria[action_index] = criteria
        if self.feature_threshold is not None:
            self.update_noisy_efficiencies(action_index=action_index)
        return criteria


class Meta_Batch_Bandit():
    def __init__(self, batch_env, bandit_instance, bandit_params=None, init_bandits=True, n_prior=None,
                 prior_mapping=None, n_init_context_batch=None, fair_exploration=False, seed=None,
                 default_action_values_dict=None, *args, **kwargs):
        batch_env.close()
        if 'horizon' in bandit_params:
            horizon = bandit_params['horizon']
        else:
            horizon = None
        if 'feature_threshold' in bandit_params:
            feature_threshold = bandit_params['feature_threshold']
        else:
            feature_threshold = None
        self.horizon = horizon
        self.feature_threshold = feature_threshold
        self.batch_env = batch_env
        self.n_actions = self.batch_env.n_actions
        self.batch_env.batch_generator.n_actions = self.batch_env.n_actions  # to avoid sampling problems
        self.clusters = self.batch_env.batch_generator.clusters
        self.fair_exploration = fair_exploration
        self.farmer_statistics = None
        self.step_seeds = None
        self.bandit_instance = bandit_instance
        if bandit_params is None:
            bandit_params = {}
        if n_init_context_batch is None:
            n_init_context_batch = self.batch_env.batch_generator.batch_size
        if default_action_values_dict is None:
            default_action_values_dict = {}
        self.default_action_values_dict = default_action_values_dict
        self.n_init_context_batch = n_init_context_batch  # batch size for init
        self.n_prior = n_prior
        self.prior_mapping = prior_mapping
        self.bandit_params = bandit_params
        self.bandit_dic = None
        self.rewards = [[] for _ in range(self.n_actions)]
        self.empirical_gaps = {cluster: [[] for _ in range(self.n_actions)] for cluster in self.clusters}
        self.batch_action_history = []
        self.batch_context_history = []
        self.batch_reward_history = []
        self.batch_efficiency_history = []
        self.pulls = np.zeros(self.n_actions)
        self.choice_criteria = np.amax
        self.regrets = None
        self.done = False
        self.T = 0
        self.t = 0
        self.seed = seed
        self.set_seed()
        if init_bandits:
            self.instantiate_bandits()

    def instantiate_bandits(self):
        bandit_dic = {}
        batch_context_history_list = []
        batch_action_history_list = []
        batch_reward_history_list = []
        batch_efficiency_history_list = []
        self.batch_env.close()
        for atomic_cluster in tqdm.tqdm(self.clusters, position=1, colour='red', desc='bandit init'):
            atomic_contexts = self.batch_env.batch_generator.cluster_to_context_mapping[atomic_cluster]
            batch_env = deepcopy(self.batch_env)
            batch_env.set_cluster_restriction(atomic_contexts)
            atomic_contexts = batch_env.batch_generator.cluster_to_context_mapping[atomic_cluster]
            batch_env.batch_generator.farmers_dict = self.batch_env.batch_generator.farmers_dict  # share the same farmers
            bandit = self.bandit_instance(cluster=atomic_cluster,
                                          batch_env=batch_env,
                                          atomic_contexts=atomic_contexts,
                                          **self.bandit_params)
            if self.n_prior is not None:
                self.batch_env.load_samples()
                self.initialize_prior(bandit)
                self.batch_env.del_samples()
            farmer_dict = self.batch_env.batch_generator.generate_farmer_batch_init(dict=True)
            action_indexes, rewards, efficiencies, contexts, batch_farmer_init = bandit.initialize(
                farmer_init_dict=farmer_dict, seeds=self.step_seeds)
            for action_index, reward, context in zip(action_indexes, rewards, contexts):
                self.rewards[action_index].append(reward)
                self.pulls[action_index] += 1
            for farmer, action_index, reward in zip(batch_farmer_init, action_indexes, rewards):  # update individuals
                # farmers
                farmer.update(action=action_index,
                              reward=reward)
            self.t += len(action_indexes)
            batch_context_history_list.extend(contexts)
            batch_action_history_list.extend(action_indexes)
            batch_reward_history_list.extend(rewards)
            batch_efficiency_history_list.extend(efficiencies)
            bandit_dic[atomic_cluster] = bandit
        self.batch_context_history.append(batch_context_history_list)
        self.batch_action_history.append(batch_action_history_list)
        self.batch_reward_history.append(batch_reward_history_list)
        self.batch_efficiency_history.append(batch_efficiency_history_list)
        self.T += 1
        self.check_T()
        self.bandit_dic = bandit_dic
        self.update_empirical_gaps()

    def initialize_prior(self, bandit):
        assert self.n_prior is not None
        assert self.prior_mapping is not None
        samples = self.batch_env.samples
        assert self.n_prior <= len(samples[[*samples][0]][0])
        cluster_to_context_mapping = self.batch_env.batch_generator.cluster_to_context_mapping
        atomic_contexts = cluster_to_context_mapping[bandit.cluster]
        n_atomic_contexts = len(atomic_contexts)
        prior_samples = [[self.batch_env.supp[-1]] for _ in range(self.n_actions)]
        # prior_samples = [[] for _ in range(self.n_actions)]
        if n_atomic_contexts > 1:  # if several contexts are associated to a cluster, then distribute prior samples
            sample_numbers = batch_utils.utils.fixed_random_sum(n_atoms=n_atomic_contexts, n_tot=self.n_prior)
            sample_numbers = [int(sample_number) for sample_number in sample_numbers]
            for atomic_context, sample_number in zip(atomic_contexts, sample_numbers):
                source_context = self.prior_mapping[atomic_context]
                for action_index, action_samples in enumerate(samples[source_context]):
                    prior_samples[action_index].extend(action_samples[:sample_number])
        else:
            source_context = self.prior_mapping[atomic_contexts[0]]
            for action_index, action_samples in enumerate(samples[source_context]):
                prior_samples[action_index].extend(action_samples[:self.n_prior])
        for action_index, action_prior_samples in enumerate(prior_samples):
            bandit.rewards_[action_index].extend(action_prior_samples)

    # TODO: question: in case of multi-context clusters, do we distribute the prior?

    def step(self):
        batch_farmer_dict = self.batch_env.batch_generator.generate_farmer_batch_step(dict=True)
        batch_farmer_list = batch_utils.utils.flatten_dic_into_list(batch_farmer_dict)
        farmer_clusters = [farmer.cluster for farmer in batch_farmer_list]
        batch_action_history_list = []
        batch_reward_history_list = []
        batch_efficiency_history_list = []
        batch_context_history_list = []
        all_pulls = 0
        for farmer_clusters in np.unique(farmer_clusters):
            bandit = self.bandit_dic[farmer_clusters]
            farmers_in_cluster_dict = self.batch_env.batch_generator.get_farmers_in_cluster(
                farmer_dict=batch_farmer_dict,
                cluster=farmer_clusters)
            farmers_in_cluster_list = batch_utils.utils.flatten_dic_into_list(farmers_in_cluster_dict)
            farmers_in_cluster_contexts = self.batch_env.batch_generator.get_contexts_from_farmer_batch(
                farmers_in_cluster_list)
            farmers_in_cluster_ingenos = self.batch_env.batch_generator.get_ingeno_from_farmer_batch(
                farmers_in_cluster_list)
            action_indexes, rewards, efficiences, _ = bandit.step(contexts=farmers_in_cluster_contexts,
                                                                  ingenos=farmers_in_cluster_ingenos,
                                                                  seeds=self.step_seeds)
            batch_efficiency_history_list.extend(efficiences)
            batch_context_history_list.extend(farmers_in_cluster_contexts)
            batch_action_history_list.extend(action_indexes)
            batch_reward_history_list.extend(rewards)
            # if hasattr(bandit, 'cycle_length_history'):
            #     cycle_lengths.append(bandit.cycle_length_history)
            if self.fair_exploration:  # base on empirical gaps at t - 1
                farmers_in_cluster_contexts, action_indexes, rewards, farmers_in_cluster_list = self.get_fair_exploration(
                    contexts=farmers_in_cluster_contexts,
                    actions=action_indexes,
                    rewards=rewards,
                    farmer_dict=farmers_in_cluster_dict)
            for action_index, reward, context in zip(action_indexes, rewards, farmers_in_cluster_contexts):
                self.rewards[action_index].append(reward)
                self.pulls[action_index] += 1
                all_pulls += 1
            self.update_empirical_gaps()
            for farmer, action_index, reward in zip(farmers_in_cluster_list, action_indexes, rewards):  # update
                # individual farmers
                farmer.update(action=action_index,
                              reward=reward)
        self.batch_context_history.append(batch_context_history_list)
        self.batch_reward_history.append(batch_reward_history_list)
        self.batch_action_history.append(batch_action_history_list)
        self.batch_efficiency_history.append(batch_efficiency_history_list)
        self.t += all_pulls
        self.T += 1
        self.check_T()

    def get_regret(self, normalize=True):
        # TODO: in case a cluster contains multiple contexts
        regrets = [0]  # 0 at T == 0
        context_history = self.batch_context_history
        action_history = self.batch_action_history
        for contexts, action_indexes in zip(context_history, action_history):
            regret = 0
            for context, action_index in zip(contexts, action_indexes):
                mean_efficency = self.batch_env.statistics[context]['all']['efficiency']['mean'][action_index]
                if self.feature_threshold is not None:
                    is_eligible = mean_efficency >= self.feature_threshold
                else:
                    is_eligible = True
                regret += self.batch_env.true_gaps[context]['all'][action_index]
                if not is_eligible:
                    regret += 1000
            if normalize:
                regret /= len(contexts)
            regrets.append(regret)
        regrets = np.cumsum(regrets)
        return regrets

    def update_empirical_gaps(self):
        # TODO add sliding window
        for cluster in [*self.bandit_dic]:  # empirical gaps at cluster level
            bandit = self.bandit_dic[cluster]
            rewards = bandit.rewards
            cluster_cvars_emp = []
            for action_index, rewards in enumerate(rewards):
                assert len(rewards) > 0
                if action_index in bandit.default_action_indexes:
                    cvar_emp = bandit.default_action_values_dict[action_index]  # if default action, then true value
                else:
                    cvar_emp = moment_empirical_cvar(samples=rewards, alpha=self.batch_env.alpha)
                cluster_cvars_emp.append(cvar_emp)
            cvars = np.asarray(cluster_cvars_emp)
            self.empirical_gaps[cluster] = cvars.max() - cvars

    def get_fair_exploration(self, contexts, actions, rewards, farmer_dict):  # distribute actions across
        cluster_dic = {}
        all_ordered_clusters = []
        all_ordered_actions = []
        all_ordered_rewards = []
        # First for each cluster we order the actions and rewards according their increasing empirical gaps i.e.,
        # decreasing cvar
        for context, action, reward in zip(contexts, actions, rewards):
            cluster = self.batch_env.batch_generator.context_to_cluster_mapping[context]
            if cluster not in cluster_dic:
                cluster_dic[cluster] = {'action': [], 'reward': []}
            cluster_dic[cluster]['action'].append(action)
            cluster_dic[cluster]['reward'].append(reward)
        for cluster in [*cluster_dic]:
            empirical_gaps = self.empirical_gaps[cluster]
            ordered_action_indexes = [action_index for (empirical_gap, action_index) in
                                      sorted(zip(empirical_gaps, range(self.n_actions)), key=lambda x: x[0])]
            order_action_index_table = {item: index for index, item in enumerate(ordered_action_indexes)}
            actions = cluster_dic[cluster]['action']
            rewards = cluster_dic[cluster]['reward']
            ordered_actions_rewards = [(action, reward) for (action, reward) in
                                       sorted(zip(actions, rewards), key=lambda x: order_action_index_table[x[0]])]
            ordered_actions, ordered_rewards = list(zip(*ordered_actions_rewards))  # unzip
            all_ordered_actions.extend(ordered_actions)
            all_ordered_rewards.extend(ordered_rewards)
            all_ordered_clusters.extend([cluster for _ in range(len(actions))])
        # In a second time, we order farmers by cluster according their decreasing empirical regret
        all_ordered_farmers = []
        for cluster in [*cluster_dic]:
            farmers_in_cluster_dict = self.batch_env.batch_generator.get_farmers_in_cluster(farmer_dict=farmer_dict,
                                                                                            cluster=cluster)
            farmers_in_cluster_list = batch_utils.utils.flatten_dic_into_list(farmers_in_cluster_dict)
            for farmer in farmers_in_cluster_list:
                farmer.get_farmer_regrets(empirical_gaps=self.empirical_gaps, true_gaps=self.batch_env.true_gaps)
            empirical_regrets = [farmer.empirical_regret for farmer in farmers_in_cluster_list]
            farmers_in_cluster_list = [farmer for (empirical_regret, farmer) in
                                       sorted(zip(empirical_regrets, farmers_in_cluster_list),
                                              key=lambda pair: pair[0])]
            # order farmer according to their increasing empiricial gap
            farmers_in_cluster_list = farmers_in_cluster_list[
                                      ::-1]  # reverse order to have decreasing empirical regrets
            all_ordered_farmers.extend(farmers_in_cluster_list)
        return all_ordered_clusters, all_ordered_actions, all_ordered_rewards, all_ordered_farmers

    def get_farmer_statistics(self):
        all_farmers_dic = self.batch_env.batch_generator.farmers_dict
        all_farmers_list = batch_utils.utils.flatten_dic_into_list(all_farmers_dic)
        farmer_statistics = {context: {'empirical_regret': [], 'true_regret': []} for context in
                             [*all_farmers_dic]}
        for farmer in all_farmers_list:
            if len(farmer.actions) == self.horizon:  # show individual regret for farmers that followed recommendations
                # the whole horizon
                farmer.get_farmer_regrets(empirical_gaps=self.empirical_gaps, true_gaps=self.batch_env.true_gaps)
                farmer_statistics[farmer.cluster]['empirical_regret'].append(
                    farmer.empirical_regret)  # empirical regret
                # defined at cluster level
                farmer_statistics[farmer.context]['true_regret'].append(farmer.true_regret)  # true regret defined at
                # context level
        self.farmer_statistics = farmer_statistics
        return farmer_statistics

    def check_T(self):
        if self.T == self.horizon:  # T starts at 0!!
            self.done = True

    def close(self):
        self.batch_env.close()
        if self.bandit_dic is not None:
            for atomic_cluster in self.clusters:
                self.bandit_dic[atomic_cluster].close()

    def reset_hard(self, seed=None):
        self.close()
        self.set_seed(seed=seed)
        self.batch_env.reset_hard(seed=seed)
        self.reset_attributes()
        self.instantiate_bandits()

    def reset(self, seed=None):
        """
        !!! system atomic_env.reset() in batch_env.step()
        """
        self.close()  # close active environments
        self.set_seed(seed=seed)
        self.batch_env.reset(seed=seed)
        self.reset_attributes()
        self.instantiate_bandits()

    def reset_attributes(self):
        self.rewards = [[] for _ in range(self.n_actions)]
        self.empirical_gaps = {atomic_context: [[] for _ in range(self.n_actions)] for atomic_context in
                               self.batch_env.batch_generator.atomic_contexts}
        self.batch_action_history = []
        self.batch_context_history = []
        self.batch_reward_history = []
        self.farmer_statistics = None
        self.pulls = np.zeros(self.n_actions)
        self.choice_criteria = np.amax
        self.regrets = None
        self.T = 0
        self.t = 0
        self.done = False

    def set_seed(self, seed=None):
        if seed is None:
            seed = self.seed
        else:
            self.bandit_params['seed'] = seed
            self.seed = seed
        np.random.seed(seed)
        n_steps_upper_bound = self.horizon * self.batch_env.batch_generator.max_batch_size
        step_seeds = np.random.choice(range(1000000), size=n_steps_upper_bound, replace=False)
        self.step_seeds = itertools.cycle(step_seeds)
        if self.bandit_dic is not None:
            for atomic_cluster in self.clusters:
                self.bandit_dic[atomic_cluster].set_seed(seed=seed)
        self.batch_env.set_seed(seed=seed)  # reopens an env
