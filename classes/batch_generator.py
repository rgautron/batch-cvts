import batch_utils
import numpy as np
import pdb


class Batch_Generator():
    def __init__(self, atomic_contexts, max_batch_size, n_actions, ingenos, atomic_context_probability_dict,
                 default_action_values_dict=None, clustering_function=None, min_batch_size=None, population_size=None,
                 n_batch_window=None, seed=None, init=True):
        self.seed = seed
        self.set_seed()
        self.atomic_contexts = atomic_contexts
        self.atomic_context_probabilities = [atomic_context_probability_dict[atomic_context] for atomic_context in
                                             atomic_contexts]
        self.atomic_context_probabilities[-1] = 1 - np.sum(self.atomic_context_probabilities[:-1])
        self.ingenos = ingenos
        self.max_batch_size = max_batch_size
        self.min_batch_size = min_batch_size
        if population_size is None:
            population_size = max_batch_size
        else:
            assert max_batch_size <= population_size
        if min_batch_size is not None and max_batch_size is not None:
            assert max_batch_size >= min_batch_size
        self.population_size = population_size
        self.fixed_batch_size = min_batch_size is None  # if min_batch_size not specified, we assume a fixed batch size
        self.farmers_dict = None
        self.batch_size = None
        self.batch_contexts = None
        self.n_actions = n_actions
        self.batch_farmers = []
        self.n_batch_window = n_batch_window
        if self.fixed_batch_size:
            self.batch_size = self.max_batch_size
        if clustering_function is None:
            clustering_function = self.default_clustering_function
        self.clustering_function = clustering_function
        self.clusters = [clustering_function(atomic_context) for atomic_context in atomic_contexts]
        self.context_to_cluster_mapping = {atomic_context: clustering_function(atomic_context) for atomic_context
                                           in self.atomic_contexts}
        self.cluster_to_context_mapping = batch_utils.utils.invert_dict(self.context_to_cluster_mapping)
        if default_action_values_dict is None:
            default_action_values_dict = {}
            n_available_actions_by_cluster = [self.n_actions for _ in self.clusters]
        else:
            n_available_actions_by_cluster = [self.n_actions - len([*default_action_values_dict[cluster]]) for cluster
                                              in self.clusters]
        self.default_action_values_dict = default_action_values_dict
        self.n_available_actions_by_cluster = n_available_actions_by_cluster
        if init:
            self.populate_farmer_pool(n_batch_window=n_batch_window)

    def populate_farmer_pool(self, n_batch_window=None):
        condition = False
        assert self.max_batch_size >= sum(self.n_available_actions_by_cluster)
        while not condition:
            contexts = np.random.choice(self.atomic_contexts, size=self.population_size,
                                        p=self.atomic_context_probabilities)
            clusters = [self.context_to_cluster_mapping[context] for context in contexts]
            cluster_count_dict = batch_utils.utils.list_to_count_dic(clusters)
            for cluster in self.clusters:
                if cluster not in cluster_count_dict:
                    cluster_count_dict[cluster] = 0
            cluster_count_list = batch_utils.utils.flatten_dic_into_list(cluster_count_dict)
            condition = np.all(cluster_count_list >= self.n_available_actions_by_cluster)  # ensure that we have enough
            # farmers from each cluster
        self.farmers_dict = {atomic_context: [] for atomic_context in np.unique(contexts)}
        for context, cluster in zip(contexts, clusters):
            ingeno = np.random.choice(self.ingenos)  # assign random cultivar
            farmer = Farmer(context=context, cluster=cluster, ingeno=ingeno, n_batch_window=n_batch_window)
            self.farmers_dict[context].append(farmer)

    def generate_farmer_batch_init(self, dict=True):
        n_available_actions_tot = sum(self.n_available_actions_by_cluster)
        if not self.fixed_batch_size:
            assert self.min_batch_size >= n_available_actions_tot
            self.batch_size = np.random.randint(low=self.min_batch_size, high=self.max_batch_size)
            print('self.batch_size ', self.batch_size)
        else:
            assert self.max_batch_size >= n_available_actions_tot
        farmer_list = batch_utils.utils.flatten_dic_into_list(self.farmers_dict)
        if len(farmer_list) == self.batch_size:
            batch_farmers = farmer_list
        else:
            condition = False
            while not condition:  # we need to ensure that we have enough farmers by cluster!
                batch_farmers = np.random.choice(farmer_list, size=self.batch_size, replace=False)
                farmers_in_cluster_count_dic = self.get_farmer_cluster_count(batch_farmers)
                for cluster in self.clusters:
                    if cluster not in farmers_in_cluster_count_dic:
                        farmers_in_cluster_count_dic[cluster] = 0  # add missing clusters in sampling in case
                farmers_in_cluster_count_list = batch_utils.utils.flatten_dic_into_list(farmers_in_cluster_count_dic)
                condition = np.all(np.asarray(farmers_in_cluster_count_list) >=
                                   np.asarray(self.n_available_actions_by_cluster))
        self.batch_farmers = batch_farmers
        if dict:
            batch_dict = {}
            for farmer in batch_farmers:
                context = farmer.context
                if context not in batch_dict:
                    batch_dict[context] = []
                batch_dict[context].append(farmer)
            return batch_dict
        return batch_farmers

    def generate_farmer_batch_step(self, dict=True):
        if not self.fixed_batch_size:
            self.batch_size = np.random.randint(low=self.min_batch_size, high=self.max_batch_size)
        farmer_list = batch_utils.utils.flatten_dic_into_list(self.farmers_dict)
        if len(farmer_list) == self.batch_size:
            batch_farmers = farmer_list
        else:
            batch_farmers = np.random.choice(farmer_list, size=self.batch_size, replace=False)
            # np.random.shuffle(farmer_list)  # not necessary because not purely sequential later
        self.batch_farmers = batch_farmers
        if dict:
            batch_dict = {}
            for farmer in batch_farmers:
                context = farmer.context
                if context not in batch_dict:
                    batch_dict[context] = []
                batch_dict[context].append(farmer)
            return batch_dict
        return batch_farmers

    def farmer_sampling_bandit_init(self, cluster, farmers_init_dict, available_action_index,
                                    n_init_context_batch=None):
        """
        Ensure that for the given cluster, we have at least n_actions farmers in intialization
        """
        farmers_in_cluster_dic = self.get_farmers_in_cluster(farmer_dict=farmers_init_dict, cluster=cluster)
        farmers_in_cluster_list = batch_utils.utils.flatten_dic_into_list(farmers_in_cluster_dic)
        assert len(farmers_in_cluster_list) >= len(available_action_index)
        if n_init_context_batch is None or n_init_context_batch == len(farmers_in_cluster_list):  # no sampling to do
            farmers_init_list = farmers_in_cluster_list
        else:
            assert n_init_context_batch <= len(farmers_in_cluster_list)
            farmers_init_list = np.random.choice(farmers_in_cluster_list, size=n_init_context_batch, replace=False)
        # print('lenght farmers_init_list: ', len(farmers_init_list))
        return farmers_init_list

    def get_contexts_from_farmer_batch(self, farmers_list=None, dict=False):
        if farmers_list is None:
            farmers_list = self.batch_farmers
        context_list = [farmer.context for farmer in farmers_list]
        if dict:
            return batch_utils.utils.list_to_count_dic(context_list)
        return context_list

    def get_ingeno_from_farmer_batch(self, farmers_list=None, dict=False):
        if farmers_list is None:
            farmers_list = self.batch_farmers
        ingeno_list = [farmer.ingeno for farmer in farmers_list]
        if dict:
            return batch_utils.utils.list_to_count_dic(ingeno_list)
        return ingeno_list

    def get_farmers_in_cluster(self, farmer_dict, cluster):
        contexts_in_cluster = self.cluster_to_context_mapping[cluster]
        farmers_in_cluster = {context: farmer_dict[context] for context in [*farmer_dict] if
                              context in contexts_in_cluster}  # subset of the dictionary for considered cluster
        return farmers_in_cluster

    def get_farmer_cluster_count(self, farmer_list):
        farmer_clusters = [farmer.cluster for farmer in farmer_list]
        cluster_count = batch_utils.utils.list_to_count_dic(farmer_clusters)
        return cluster_count

    def update_farmers(self, farmers, actions, rewards, T):
        for farmer, action, reward in zip(farmers, actions, rewards):
            farmer.update(action=action,
                          reward=reward)

    def contexts_to_clusters(self, batch):
        if self.clustering_function is None:
            raise ValueError('please provide the "clustering_function" argument to the BatchGenerator')
        if isinstance(batch, dict):
            batch_contexts = [*batch]
            return {self.clustering_function(batch_context): batch[batch_context] for batch_context in batch_contexts}
        clusters = []
        for batch_contexts in batch:
            clusters.append(self.clustering_function(batch_contexts))
        return clusters

    @staticmethod
    def default_clustering_function(context):
        return context

    def set_seed(self, seed=None):
        if seed is None:
            seed = self.seed
        else:
            self.seed = seed
        np.random.seed(seed)

    def reset(self, seed=None):
        self.set_seed(seed=seed)
        self.farmers_dict = None
        self.batch_contexts = None
        self.batch_farmers = []
        self.populate_farmer_pool(n_batch_window=self.n_batch_window)


class Farmer():
    def __init__(self, context, cluster, ingeno, n_batch_window=None):
        self.context = context
        self.cluster = cluster
        self.ingeno = ingeno  # cultivar
        self.rewards = []
        self.T = []
        self.actions = []
        self.empirical_regret = None
        self.true_regret = None
        self.n_batch_window = n_batch_window

    def update(self, action, reward):
        self.actions.append(action)
        self.rewards.append(reward)

    def get_farmer_regrets(self, empirical_gaps, true_gaps):
        if self.n_batch_window is not None:
            actions = self.slice_actions()
        else:
            actions = np.asarray(self.actions)
        if len(actions) == 0:  # empty list i.e. no recommendations so far
            empirical_regret = 0
            true_regret = 0
        else:
            empirical_regret = (np.asarray(empirical_gaps[self.cluster])[actions]).sum()
            # true_regret = (np.asarray(true_gaps[self.context][self.ingeno])[actions]).sum()
            true_regret = (np.asarray(true_gaps[self.context]['all'])[actions]).sum()
        self.empirical_regret = empirical_regret
        self.true_regret = true_regret
        return empirical_regret, true_regret

    def slice_actions(self):  # if sliding window for non stationarity
        if self.n_batch_window is not None and len(self.actions) > self.n_batch_window:  # Python allows short
            return self.actions[:-self.n_batch_window]
        return self.actions

    def reset(self):
        self.rewards = []
        self.T = []
        self.actions = []
        self.empirical_regret = None