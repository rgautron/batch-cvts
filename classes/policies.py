import pdb
from copy import deepcopy

class Fertilizer_Policy():
    def __init__(self, rainfall_threshold, nsi_threshold):
        self.policy_dic = {
            0: self.policy_0,
            1: self.policy_1,
            2: self.policy_2,
            3: self.policy_3,
            4: self.policy_4,
            5: self.policy_5,
            6: self.policy_6,
            7: self.policy_7,
            8: self.policy_8,
            9: self.policy_9,
        }
        self.action_values = [*self.policy_dic]
        self.init_history = {'das_rainfall': 0, 'rainfall_threshold': False}
        self.history = deepcopy(self.init_history)
        self.rainfall_threshold = rainfall_threshold
        self.nsi_threshold = nsi_threshold

    def play_policy(self, policy_index, observation):
        if policy_index == -1:  # null policy for sampling
            return {'anfer': 0}
        assert policy_index in self.policy_dic
        self.history['das_rainfall'] += observation['rain']
        return self.policy_dic[policy_index](observation)

    @staticmethod
    def policy_common(dap):
        if dap == 15:
            return {'anfer': 15}

    def policy_0(self, observation):
        dap = observation['dap']
        self.policy_common(dap=dap)
        if dap == 30:
            anfer = 120
        else:
            anfer = 0
        return {'anfer': anfer}

    def policy_1(self, observation):
        dap = observation['dap']
        nsi = 1 - observation['nstres']
        self.policy_common(dap=dap)
        base_rate = 120
        if dap == 30:
            if self.check_nsi_threshold(nsi=nsi):
                anfer = base_rate
            else:
                # anfer = base_rate / 3
                anfer = 0
        else:
            anfer = 0
        return {'anfer': anfer}

    def policy_2(self, observation):
        dap = observation['dap']
        self.policy_common(dap=dap)
        base_rate = 120
        if dap == 30:
            if self.check_rainfall_threshold(dap=dap):
                anfer = base_rate
            else:
                # anfer = base_rate / 3
                anfer = 0
        else:
            anfer = 0
        return {'anfer': anfer}

    def policy_3(self, observation):
        dap = observation['dap']
        nsi = 1 - observation['nstres']
        self.policy_common(dap=dap)
        base_rate = 120
        if dap == 30:
            if self.check_rainfall_threshold(dap=dap) and self.check_nsi_threshold(nsi=nsi):
                anfer = base_rate
            else:
                # anfer = base_rate / 3
                anfer = 0
        else:
            anfer = 0
        return {'anfer': anfer}

    def policy_4(self, observation):
        dap = observation['dap']
        self.policy_common(dap=dap)
        if dap == 30:
            anfer = 60
        elif dap == 45:
            anfer = 60
        else:
            anfer = 0
        return {'anfer': anfer}

    def policy_5(self, observation):
        dap = observation['dap']
        nsi = 1 - observation['nstres']
        self.policy_common(dap=dap)
        base_rate = 60
        if dap == 30:
            if self.check_nsi_threshold(nsi=nsi):
                anfer = base_rate
            else:
                # anfer = base_rate / 3
                anfer = 0
        elif dap == 45:
            if self.check_nsi_threshold(nsi=nsi):
                anfer = base_rate
            else:
                # anfer = base_rate / 3
                anfer = 0
        else:
            anfer = 0
        return {'anfer': anfer}

    def policy_6(self, observation):
        dap = observation['dap']
        self.policy_common(dap=dap)
        base_rate = 60
        if dap == 30:
            if self.check_rainfall_threshold(dap=dap):
                anfer = base_rate
            else:
                # anfer = base_rate / 3
                anfer = 0
        elif dap == 45:
            if self.check_rainfall_threshold(dap=dap):
                anfer = base_rate
            else:
                # anfer = base_rate / 3
                anfer = 0
        else:
            anfer = 0
        return {'anfer': anfer}

    def policy_7(self, observation):
        dap = observation['dap']
        nsi = 1 - observation['nstres']
        self.policy_common(dap=dap)
        base_rate = 60
        if dap == 30:
            if self.check_rainfall_threshold(dap=dap) and self.check_nsi_threshold(nsi=nsi):
                anfer = base_rate
            else:
                # anfer = base_rate / 3
                anfer = 0
        elif dap == 45:
            if self.check_rainfall_threshold(dap=dap) and self.check_nsi_threshold(nsi=nsi):
                anfer = base_rate
            else:
                # anfer = base_rate / 3
                anfer = 0
        else:
            anfer = 0
        return {'anfer': anfer}

    def policy_8(self, observation):
        dap = observation['dap']
        if dap == 15:
            anfer = 23
        elif dap == 45:
            anfer = 47
        else:
            anfer = 0
        return {'anfer': anfer}

    def policy_9(self, observation):
        dap = observation['dap']
        if dap == 15:
            anfer = 30
        elif dap == 30:
            anfer = 60
        elif dap == 45:
            anfer = 60
        else:
            anfer = 0
        return {'anfer': anfer}

    def check_rainfall_threshold(self, dap):
        if dap == 30:
            if self.history['das_rainfall'] >= self.rainfall_threshold:
                self.history['rainfall_threshold'] = True
        return self.history['rainfall_threshold']

    def check_nsi_threshold(self, nsi):
        if nsi >= self.nsi_threshold:
            stressed = False
        else:
            stressed = True
        return stressed

    def reset(self):
        self.history = deepcopy(self.init_history)
