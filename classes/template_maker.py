from dssatUtils.dssatParsing import dssatParsers
from gym_dssat_pdi.envs.utils import utils as dssat_utils
import batch_utils
import numpy as np
import pdb

class Template_Maker():
    def __init__(self, dssat_path, parent_fileX_template_path, filling_dictionaries, child_template_saving_paths,
                 soil_file_path, default_snh4=.6, default_sno3=3, default_pawc_p=0, sno3_bounds=None, snh4_bounds=None,
                 random_inital_nitrogen=False, random_cultivar_list=None, initial_nitrogen_dic=None, seed=None):
        self.dssat_path = dssat_path
        self.parent_template_path = parent_fileX_template_path
        self.child_template_paths = child_template_saving_paths
        self.soil_file_path = soil_file_path
        self.filling_dictionaries = filling_dictionaries
        self.initial_nitrogen_dic = initial_nitrogen_dic
        self.default_snh4 = default_snh4
        self.default_sno3 = default_sno3
        if snh4_bounds is None:
            snh4_bounds = [.1, .9]
        self.snh4_bounds = snh4_bounds
        if sno3_bounds is None:
            sno3_bounds = [.1, 8]
        self.sno3_bounds = sno3_bounds
        self.default_pawc_p = default_pawc_p
        self.random_inital_nitrogen = random_inital_nitrogen
        self.random_cultivar_list = random_cultivar_list
        self.seed = None
        self.set_seed(seed=seed)

    def make_child_templates(self, ingeno=None, context=None):
        parent_template = dssat_utils._load_fileX_template(fileX_template_path=self.parent_template_path)
        self.complete_filling_dicts(ingeno=ingeno, context=context)
        if context is None:
            contexts = [*self.filling_dictionaries]
            for context in contexts:
                ingenos = [*self.filling_dictionaries[context]]
                for ingeno in ingenos:
                    output_path = self.child_template_paths[context][ingeno]
                    filling_dictionary = self.filling_dictionaries[context][ingeno]
                    dssat_utils._write_template_from_string(value_dic=filling_dictionary,
                                                      template_string=parent_template,
                                                      saving_path=output_path)
        else:  # if one wants just the template for a given context and cultivar
            assert ingeno is not None
            filling_dictionary = self.filling_dictionaries[context][ingeno]
            filled_string_template = dssat_utils._fill_template_from_string(value_dic=filling_dictionary,
                                                                      template_string=parent_template)
            return filled_string_template

    def complete_filling_dicts(self, ingeno=None, context=None):
        parser = dssatParsers.DssatIntegrationParser(soil_file_path=self.soil_file_path)
        soil_dic = parser.get_dic_from_file('soil')
        if not self.random_inital_nitrogen:
            if self.initial_nitrogen_dic is None:
                snh4_value = self.default_snh4
                sno3_value = self.default_sno3
        pawc_p = self.default_pawc_p
        if context is None:
            contexts = [*self.filling_dictionaries]
        else:
            contexts = [context]
        if ingeno is None:
            ingenos = [*self.filling_dictionaries[[*self.filling_dictionaries][0]]]
        else:
            ingenos = [ingeno]
        for context in contexts:
            for ingeno in ingenos:
                filling_dictionary = self.filling_dictionaries[context][ingeno]
                id_soil = filling_dictionary['id_soil']
                if self.random_inital_nitrogen:
                    sno3_value = np.random.uniform(low=self.sno3_bounds[0], high=self.sno3_bounds[1])
                    snh4_value = np.random.uniform(low=self.snh4_bounds[0], high=self.snh4_bounds[1])
                elif self.initial_nitrogen_dic is not None:
                    sno3_value = self.initial_nitrogen_dic[id_soil]['sno3']
                    snh4_value = self.initial_nitrogen_dic[id_soil]['snh4']
                all_soil_params = soil_dic[context]
                snh4 = [snh4_value for _ in all_soil_params['SLB']]
                sno3 = [sno3_value for _ in all_soil_params['SLB']]
                sh2o = all_soil_params['SLLL'] * (1 - pawc_p) + pawc_p * all_soil_params['SDUL']
                icbl = all_soil_params['SLB']
                ic_dic = {'snh4': snh4, 'sno3': sno3, 'sh2o': sh2o, 'icbl': icbl}
                filling_dictionary['ic_dics'] = batch_utils.utils.transpose_ic_dic(ic_dic)
                if self.random_cultivar_list is not None:
                    ingeno = np.random.choice(self.random_cultivar_list)
                self.filling_dictionaries[context][ingeno] = filling_dictionary

    def set_seed(self, seed):
        self.seed = seed
        np.random.seed(seed)