import pdb


class Fertilizer_Policy():
    def __init__(self):
        self.policy_dic = {
            0: self.policy_0,
            1: self.policy_1,
            2: self.policy_2,
            3: self.policy_3,
        }
        self.action_values = [*self.policy_dic]
        self.fertilizer_flags = {'vstage': range(1, 31), 'istage': range(10)}
        self.flag_dict = {}
        self.init_flags()

    def play_policy(self, policy_index, observation):
        assert policy_index in self.policy_dic
        return self.policy_dic[policy_index](observation)

    def policy_0(self, observation):
        policy_index = 0
        vstage = observation['vstage']
        istage = observation['istage']
        if vstage in [14, 18] and not self.flag_dict['vstage'][policy_index][vstage]:
            anfer = 25
            self.set_flags('vstage', vstage, policy_index=policy_index)
        elif istage == 5 and not self.flag_dict['istage'][policy_index][istage]:
            anfer = 40
            self.set_flags('istage', istage, policy_index=policy_index)
        else:
            anfer = 0
        return {'anfer': anfer}

    def policy_1(self, observation):
        policy_index = 1
        vstage = observation['vstage']
        istage = observation['istage']
        nsi = 1 - observation['nstres']
        # print('istage vstage dap ', istage, vstage, observation['dap'])
        if vstage in [12, 18] and nsi < .90 and not self.flag_dict['vstage'][policy_index][vstage]:
            anfer = 30
            self.set_flags('vstage', vstage, policy_index=policy_index)
        elif istage == 5 and not self.flag_dict['istage'][policy_index][istage]:
            anfer = 45
            self.set_flags('istage', istage, policy_index=policy_index)
        else:
            anfer = 0
        return {'anfer': anfer}

    def policy_2(self, observation):
        policy_index = 2
        vstage = observation['vstage']
        istage = observation['istage']
        nsi = 1 - observation['nstres']
        if vstage > 18 and istage < 5 and nsi < .90 and not self.flag_dict['vstage'][policy_index][vstage]:
            anfer = 10
            self.set_flags('vstage', vstage, policy_index=policy_index)
        elif istage == 5 and nsi < .90 and not self.flag_dict['istage'][policy_index][istage] :
            anfer = 25
            self.set_flags('istage', istage, policy_index=policy_index)
        else:
            anfer = 0
        return {'anfer': anfer}

    def policy_3(self, observation):
        policy_index = 3
        vstage = observation['vstage']
        istage = observation['istage']
        nsi = 1 - observation['nstres']
        if vstage > 18 and istage <= 5 and nsi < .90 and not self.flag_dict['vstage'][policy_index][vstage]:
            anfer = 20
            self.set_flags('vstage', vstage, policy_index=policy_index)
        else:
            anfer = 0
        return {'anfer': anfer}

    def init_flags(self):
        flag_dict = {}
        for flag_feature in [*self.fertilizer_flags]:
            flag_dict[flag_feature] = {}
            for policy_index in self.action_values:
                flag_dict[flag_feature][policy_index] = {feature_value: False for feature_value in
                                                         self.fertilizer_flags[flag_feature]}
        self.flag_dict = flag_dict

    def set_flags(self, flag_feature, feature_value, policy_index):
        self.flag_dict[flag_feature][policy_index][feature_value] = True

    def reset(self):
        self.init_flags()
