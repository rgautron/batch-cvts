from gym_dssat_pdi.envs.utils import utils
from utils import data_visualization
from classes import policies
from utils import utils as batch_utils
from classes.batch_generator import Batch_Generator

import os
from pprint import pprint
from copy import deepcopy
import pickle
import numpy
import gym
import logging
import faulthandler
import pathlib
faulthandler.enable()



def interact_with_env(env, policy, verbose=True):
    interactions = []
    while not env.done:
        observation = env.observation
        action = policy(observation)
        if verbose:
            # pprint(f'observation: {observation}')
            pprint(f'yrdoy : {observation["yrdoy"]} -> fertilizing {action["anfer"]} kgN/ha')
            # print(f'sw: {env._state["sw"]}')
        res = env.step(action)
        new_state, reward, done, info = res
        if new_state is not None:
            interactions.append(new_state)
    return interactions

def interact_with_env_(env, policy, feature_name, verbose=True):
    interactions = interact_with_env(env, policy, verbose=verbose)
    env.reset()
    value = interactions[-1][feature_name]
    return value

def get_samples(env, policy, n_samples, feature_name):
    samples = []
    print_step = n_samples // 10
    for i in range(n_samples):
        value = interact_with_env_(env, policy, feature_name, verbose=True)
        samples.append(value)
        if (i + 1) % print_step == 0:
            print(i + 1)
    return samples

def play_policy_on_context(policy_index, all_envs, context, feature_name):
    policies.get_policy(policy_index)
    env = all_envs[context]
    value = interact_with_env_(env, policy, feature_name, verbose=True)
    return value

if __name__ == '__main__':
    ############ EXP SETTINGS ############
    # verbose = True
    verbose = False
    # make_fileX_templates = True
    make_fileX_templates = False
    # sampling = True
    sampling = False
    # plot_dists = True
    plot_dists = False
    n_samples = int(1e3)
    feature_name = 'grnwt'

    dirs = ['./logs', './render', './output']
    for dir in dirs:
        utils.make_folder(dir)
    try:
        for file in os.scandir('../logs'):
            os.remove(file.path)
    except:
        pass
    sample_dic_saving_path = f'./output/sampling_n_{n_samples}.pkl'
    ############ TEMPLATE PREPARATION ############
    soil_ids = [f'HC_GEN00{i:02d}' for i in range(1, 28)]
    filling_dictionnaries = [{'id_soil': soil_id} for soil_id in soil_ids]
    # fileX_prefix = 'GAGR0201'
    fileX_prefix = 'UFGA8201'
    child_template_saving_paths = [f'./templates/child_fileX_templates/{fileX_prefix}_HC_GEN00{i:02d}' for i in
                                   range(1, 28)]
    parent_fileX_template_path = f'./templates/parent_fileX_templates/{fileX_prefix}_parent.jinja2'
    if make_fileX_templates:
        batch_utils.make_child_templates(parent_template_path=parent_fileX_template_path,
                                               filling_dictionaries=filling_dictionnaries,
                                               output_paths=child_template_saving_paths)
    fileX_template_path = '../dssat_files/parent_fileX_templates/GAGR0201.jinja2'
    seed = numpy.random.randint(1, 9999999)
    env_args = {
        'run_dssat_location': f'{pathlib.Path.home()}/dssat_pdi/run_dssat',
        'log_saving_path': './logs/dssat-pdi.log',
        'mode': 'fertilization',
        'seed': seed,
        'random_weather': True,
        'fileX_template_path': fileX_template_path,
        'experiment_number': 1,
    }
    all_env_args = {}
    for child_template_saving_path, soil_id in zip(child_template_saving_paths, soil_ids):
        env_args_cp = deepcopy(env_args)
        env_args_cp['fileX_template_path'] = child_template_saving_path
        all_env_args[soil_id] = env_args_cp

    ############ SAMPLING ############
    hc_indexes_to_select = [22, 13]
    soils_to_select = [f'HC_GEN00{i:02d}' for i in hc_indexes_to_select]
    selected_env_args = [all_env_args[soil_to_select] for soil_to_select in soils_to_select]
    all_samples_dic = {}
    if sampling:
        try:
            for i, (env_args, soil_to_select) in enumerate(zip(selected_env_args, soils_to_select)):
                env = gym.make('gym_dssat_pdi:GymDssatPdi-v0', **env_args)
                policy_samples_dic = {}
                for policy_index in range(3):
                    policy = policies.get_policy(policy_index)
                    samples = get_samples(env, policy, n_samples, feature_name)
                    policy_samples_dic[f'policy_{policy_index}'] = samples
                    env.reset_hard()
                all_samples_dic[soil_to_select] = policy_samples_dic
                env.close()
            with open(sample_dic_saving_path, 'wb') as f_:
                pickle.dump(all_samples_dic, f_, protocol=pickle.HIGHEST_PROTOCOL)
        except Exception as e:
            logging.exception(e)
        finally:
            pass
    else:
        with open(sample_dic_saving_path, 'rb') as f_:
            all_samples_dic = pickle.load(f_)

    ############ DATA VIZ ############
    if plot_dists:
        data_visualization.plot_policies_rewards(all_samples_dic, saving_prefix='./render')

    ############ BANDITS ############
    clustering_function = lambda context: context
    batch_generator = Batch_Generator(atomic_contexts=soils_to_select, max_batch_size=500,
                                     clustering_function=clustering_function)
    batch_dict = batch_generator.get_batch_contexts(batch_dict=True)
    # batch_list = batch_generator.get_batch()
    cluster_dict = batch_generator.contexts_to_clusters(batch_dict)
    # cluster_list = batch_generator.contexts_to_clusters(batch_list)