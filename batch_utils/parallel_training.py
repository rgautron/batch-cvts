from classes.meta_batch_bandit import Meta_Batch_Bandit
import multiprocessing_on_dill as mp
import random
import sys
from tqdm import tqdm
import numpy as np
import logging
import faulthandler
from gym_dssat_pdi.envs.utils import utils as dssat_utils

faulthandler.enable()

import pdb


def _parallel_regret(args):
    meta_bandit, horizon, seed = args
    try:
        res_dic = {}
        meta_bandit.reset_hard(seed=seed)
        while not meta_bandit.done:
            meta_bandit.step()
        regret = meta_bandit.get_regret()
        farmer_statistics = meta_bandit.get_farmer_statistics()
        farmer_regrets = []
        for context in [*farmer_statistics]:
            farmer_regret = farmer_statistics[context]['true_regret']
            farmer_regrets.extend(farmer_regret)
        res_dic['regret'] = np.asarray(regret)
        res_dic['farmer_regret'] = farmer_regrets
        res_dic['rewards'] = meta_bandit.batch_reward_history
        res_dic['actions'] = meta_bandit.batch_action_history
        res_dic['contexts'] = meta_bandit.batch_context_history
        res_dic['efficiencies'] = meta_bandit.batch_efficiency_history
        return res_dic
    except Exception as e:
        print(e)
        logging.exception(e)
    finally:
        meta_bandit.close()


def get_parallel_regret_(bandit, horizon, replications):
    n_cores = mp.cpu_count()
    seeds = np.random.choice(range(1000000), size=replications, replace=False)
    args = [(bandit, horizon, seed) for seed in seeds]
    results = []
    with mp.Pool(processes=n_cores) as pool:
        for result_element in tqdm(pool.imap_unordered(_parallel_regret, args), total=len(args), desc='regret'):
            results.append(result_element)
    results = dssat_utils.transpose_dicts(results)
    return results