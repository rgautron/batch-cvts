from batch_utils.utils import pickle_to_json

if __name__ == '__main__':
    loading_path = './output/sampling_n_100000.pkl'
    saving_path = './output/sampling_n_100000.json'
    pickle_to_json(pickle_loading_path=loading_path, json_saving_path=saving_path)