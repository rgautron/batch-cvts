import matplotlib
from cvarBandits.utils.estimators import moment_empirical_cvar, cvar_ci
from concentration_lib.empirical_concentration_bounds import empirical_hedged_capital_bound, \
    empirical_small_samples_ptlm
from matplotlib import colors
from batch_utils import utils as batch_utils
import pandas as pd

matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
import matplotlib.ticker as plticker
from scipy.stats import gaussian_kde
import itertools
import seaborn as sns
import numpy as np
from copy import deepcopy
import pdb


# def plot_policies_rewards(sample_dic, saving_prefix='.'):
#     soil_keys = [*sample_dic]
#     for soil_key in soil_keys:
#         fig, ax = plt.subplots()
#         policy_dics = sample_dic[soil_key]
#         policy_keys = [*policy_dics]
#         colors = sns.color_palette('colorblind', len(policy_keys))
#         for policy_key, color in zip(policy_keys, colors):
#             samples = policy_dics[policy_key]
#             n_samples = len(samples)
#             sns.histplot(samples, element="step", stat="density", label=policy_key, ax=ax, color=color, alpha=.1,
#                          linewidth=3)
#         ax.legend()
#         plt.title(f'Soil {soil_key} with \#{n_samples} samples')
#         plt.savefig(f'{saving_prefix}/dists_{soil_key}_n_{n_samples}.pdf')

def plot_farmer_regrets(raw_results, alpha, batch_size, horizon, replications, reward_feature_layout,
                        saving_prefix='.', x_label=None, saving_path=None, bandit_names_plot_dic=None):
    bandit_names = [bandit_name for bandit_name in raw_results if bandit_name != 'info']
    reward_feature = raw_results['info']['reward_feature']
    bandit_names.sort()
    fig, ax = plt.subplots()
    palette = itertools.cycle(sns.color_palette('bright'))
    markers = itertools.cycle(['*', 'X', '^'])
    all_samples = []
    for bandit_name in bandit_names:
        samples = np.concatenate(list(raw_results[bandit_name]['farmer_regret']), axis=None)
        all_samples.extend(samples)
    support = (min(all_samples), max(all_samples))
    del all_samples
    legend_elements = []
    n_tot_farmers = 0
    for index, bandit_name in enumerate(bandit_names):
        if bandit_names_plot_dic is not None:
            bandit_name_plot = bandit_names_plot_dic[bandit_name]
        else:
            bandit_name_plot = bandit_name
        samples = np.concatenate(list(raw_results[bandit_name]['farmer_regret']), axis=None)
        n_tot_farmers += len(samples)
        x_kde = np.linspace(start=support[0], stop=support[1], num=1000)
        kde_f = gaussian_kde(samples)
        y_kde = kde_f(x_kde)
        x_kde_points_step = support[1] // 6
        x_kde_points = np.arange(start=x_kde_points_step, stop=support[1] - x_kde_points_step, step=x_kde_points_step)
        y_kde_points = kde_f(x_kde_points)
        label = bandit_name_plot
        marker = next(markers)
        dash = (1, 0, 0, 0)
        color = next(palette)
        kde_p = ax.plot(x_kde, y_kde, alpha=1, color=color, zorder=1, dashes=dash, linewidth=3)
        points_kde = ax.plot(x_kde_points, y_kde_points, color=color, marker=marker, zorder=3, markersize=12,
                             linestyle='')
        legend_element = Line2D([None], [None], label=label, dashes=dash, marker=marker, color=color, markersize=8,
                                linewidth=2)
        legend_elements.append(legend_element)
        mean = np.mean(samples)
        ax.axvline(x=mean, color=color, linestyle='-', linewidth=1.5, alpha=1)
    farmer_number = n_tot_farmers // len(bandit_names)
    mean_legend = Line2D([None], [None], color='black', lw=1.5, label='mean', linestyle='-')
    legend_elements.append(mean_legend)
    ax.set_xlim([support[0], 1.05 * support[1]])
    ax.set_xbound(lower=support[0], upper=1.05 * support[1])
    ax.legend(handles=legend_elements, loc='best')
    ax.yaxis.set_major_formatter(plticker.FormatStrFormatter('%.1e'))
    if x_label is None:
        x_label = reward_feature_layout[reward_feature]
    ax.set_xlabel(x_label)
    ax.set_ylabel('density')
    plt.title(f'Distribution of true regrets for farmers ; alpha={100 * alpha:.0f}% \n'
              f'farmer number: {farmer_number:.0f}')
    if saving_path is None:
        saving_path = f'{saving_prefix}_farmer_regret_R_{replications}_H_{horizon}_A_' \
                      f'{100 * alpha:.0f}_100.pdf'
    plt.tight_layout()
    plt.savefig(saving_path)


def plot_farmer_empirical_cvar(raw_results, alpha, batch_size, horizon, replications, bounds_dict,
                               reward_feature_layout, bandit_names_plot_dic=None, delta_ci=.1, saving_prefix='.',
                               y_label=None, saving_path=None):
    print('~~~ rendering empirical cvars ~~~')
    assert 0 < delta_ci < .5
    bandit_names = [bandit_name for bandit_name in raw_results if bandit_name != 'info']
    bandit_names.sort()
    reward_feature = raw_results['info']['reward_feature']
    supp = bounds_dict[reward_feature]
    all_colors = np.array([('navy', 'lightblue', 'steelblue'),
                           ('darkorange', 'navajowhite', 'orange'),
                           ('green', 'palegreen', 'forestgreen'),
                           ('deeppink', 'pink', 'hotpink'),
                           ('purple', 'orchid', 'blueviolet'),
                           ])
    dashes = ['dashed', 'solid', 'dotted']
    dashes = itertools.cycle(dashes)
    markers = ['*', 'X', '^']
    markers = itertools.cycle(markers)
    palette_lines = all_colors[:, 0]
    palette_lines = itertools.cycle(palette_lines)
    palette_fill = all_colors[:, 1]
    palette_fill = itertools.cycle(palette_fill)
    fig, ax = plt.subplots()
    legend_elements = []
    n_tot_batch = 0
    ylabel_feature = reward_feature_layout[reward_feature]
    csv_dic = {}
    for index, bandit_name in enumerate(bandit_names):
        if bandit_names_plot_dic is not None:
            bandit_name_plot = bandit_names_plot_dic[bandit_name]
        else:
            bandit_name_plot = bandit_name
        if index == 0:
            zorder = 2
        else:
            zorder = 1
        color_fill = next(palette_fill)
        line_color = next(palette_lines)
        marker = next(markers)
        dash = next(dashes)
        rewards_reps = raw_results[bandit_name]['rewards']
        rewards_reps = list(zip(*rewards_reps))
        cumulated_rewards = []
        for index, rewards_rep in enumerate(rewards_reps):
            rewards_rep = np.concatenate(rewards_rep, axis=None).tolist()
            if index == 0:
                cumulated_rewards.append(deepcopy(rewards_rep))
            else:
                cumulated_rewards.append(deepcopy([*cumulated_rewards[-1], *rewards_rep]))
        cvars = []
        cvars_bounds = []
        for index, cumulated_reward in enumerate(cumulated_rewards):
            n = len(cumulated_reward)
            if index == len(cumulated_rewards) - 1:
                n_tot_batch += n
            cvar = moment_empirical_cvar(cumulated_reward, alpha=alpha)
            cvars.append(cvar)
            b_t = np.sqrt(np.log(2 / delta_ci) / (2 * n))
            cvars_bound = []
            for upper in [False, True]:
                bound = cvar_ci(samples=cumulated_reward,
                                alpha=alpha,
                                b_t=b_t,
                                support=supp,
                                upper=upper
                                )
                cvars_bound.append(bound)
            cvars_bounds.append(cvars_bound)
        lower_bounds, upper_bounds = list(zip(*cvars_bounds))
        x = range(1, horizon + 1)
        ax.plot(x, cvars, color=line_color, linestyle=dash, label=bandit_name_plot, linewidth=3)
        csv_dic[bandit_name] = cvars
        ax.plot(x, lower_bounds, color=color_fill, linewidth=1, linestyle=dash, zorder=zorder)
        ax.plot(x, upper_bounds, color=color_fill, linewidth=1, linestyle=dash, zorder=zorder)
        ax.fill_between(x=x, y1=lower_bounds, y2=upper_bounds, color=color_fill, zorder=zorder - 1, alpha=1)
        x_points_step = horizon // (horizon // 2)
        x_points = range(x_points_step, horizon, x_points_step)
        common_x_indexes = [i for i, x in enumerate(x) if x in x_points]
        y_points = np.asarray(cvars)[common_x_indexes]
        points = ax.plot(x_points, y_points, color=line_color, marker=marker, zorder=zorder + 2, linestyle='',
                         markersize=10, lw=3)
        legend_element = Line2D([None], [None], lw=2, label=bandit_name_plot, linestyle=dash, marker=marker,
                                linewidth=2,
                                color=line_color, markersize=10)
        legend_elements.append(legend_element)
    mean_batch_size = n_tot_batch / (len(bandit_names) * horizon * replications)
    patch = Patch(facecolor='black', edgecolor=None, label=f'{1 - delta_ci:.0%} confidence interval', alpha=.2)
    legend_elements.append(patch)
    ax.legend(handles=legend_elements, loc='best')
    major_loc = plticker.MultipleLocator(base=2)
    minor_loc = plticker.MultipleLocator(base=1)
    ax.xaxis.set_major_locator(major_loc)
    ax.xaxis.set_minor_locator(minor_loc)
    plt.title(f'Empirical CVaR @ alpha={100 * alpha:.0f}% ; mean batch size: {np.around(mean_batch_size):.0f} \n'
              f'{replications} replications')
    if saving_path is None:
        saving_path = f'{saving_prefix}_farmer_empirical_cvar_R_{replications}_H_{horizon}_A_{100 * alpha:.0f}_100.pdf'
    ax.set_xlabel('time step T')
    if y_label is None:
        y_label = f'empirical CVaR of the {ylabel_feature}'
    ax.set_ylabel(y_label)
    plt.tight_layout()
    plt.savefig(saving_path)
    df = pd.DataFrame.from_dict(csv_dic, orient='index', columns=x)
    df.to_csv(f'{saving_path[:-4]}_line.csv')


def plot_farmer_efficiencies(raw_results, alpha, batch_size, horizon, replications, efficiency_threshold, supp,
                             bandit_names_plot_dic=None, delta_ci=.1, saving_prefix='.', saving_path=None):
    print('~~~ rendering efficiencies ~~~')
    assert 0 < delta_ci < .5
    bandit_names = [bandit_name for bandit_name in raw_results if bandit_name != 'info']
    bandit_names.sort()
    all_colors = np.array([('navy', 'lightblue', 'steelblue'),
                           ('darkorange', 'navajowhite', 'orange'),
                           ('green', 'palegreen', 'forestgreen'),
                           ('deeppink', 'pink', 'hotpink'),
                           ('purple', 'orchid', 'blueviolet'),
                           ])
    dashes = ['dashed', 'solid', 'dotted']
    dashes = itertools.cycle(dashes)
    markers = ['*', 'X', '^']
    markers = itertools.cycle(markers)
    palette_lines = all_colors[:, 0]
    palette_lines = itertools.cycle(palette_lines)
    palette_fill = all_colors[:, 1]
    palette_fill = itertools.cycle(palette_fill)
    fig, ax = plt.subplots()
    legend_elements = []
    batch_sizes = []
    for index, bandit_name in enumerate(bandit_names):
        if bandit_names_plot_dic is not None:
            bandit_name_plot = bandit_names_plot_dic[bandit_name]
        else:
            bandit_name_plot = bandit_name
        if index == 0:
            zorder = 2
        else:
            zorder = 1
        color_fill = next(palette_fill)
        line_color = next(palette_lines)
        marker = next(markers)
        dash = next(dashes)
        efficiency_reps = raw_results[bandit_name]['efficiencies']
        efficiency_reps = list(zip(*efficiency_reps))
        cumulated_efficiencies = []
        for index, efficiencies_rep in enumerate(efficiency_reps):
            efficiencies_rep = np.concatenate(efficiencies_rep, axis=None).tolist()
            if index == 0:
                cumulated_efficiencies.append(deepcopy(efficiencies_rep))
            else:
                cumulated_efficiencies.append(deepcopy([*cumulated_efficiencies[-1], *efficiencies_rep]))
        means = []
        means_bounds = []
        for index, cumulated_efficiency in enumerate(cumulated_efficiencies):
            cumulated_efficiency = np.asarray(cumulated_efficiency)
            cumulated_efficiency = cumulated_efficiency[~np.isnan(cumulated_efficiency)]
            n = len(cumulated_efficiency)
            if index == len(cumulated_efficiencies) - 1:
                batch_sizes.append(n)
            mean = np.mean(cumulated_efficiency)
            means.append(mean)
            mean_bounds = []
            for side in ['lower', 'upper']:
                try:
                    bound = empirical_hedged_capital_bound(samples=cumulated_efficiency,
                                                           delta=delta_ci / 2,
                                                           upper_bound=supp[-1],
                                                           lower_bound=supp[0],
                                                           side=side,
                                                           mode='mean')
                except Exception as e:
                    print(e)
                    pdb.set_trace()
                mean_bounds.append(bound)
            means_bounds.append(mean_bounds)
        lower_bounds, upper_bounds = list(zip(*means_bounds))
        x = range(1, horizon + 1)  # TODO: check Ts
        means = np.asarray(means)
        x = np.array(range(horizon + 1))
        lower_bounds = np.asarray(lower_bounds)
        upper_bounds = np.asarray(upper_bounds)
        ax.plot(x, means, color=line_color, linestyle=dash, label=bandit_name_plot, linewidth=3)
        ax.plot(x, means - lower_bounds, color=color_fill, linewidth=1, linestyle=dash, zorder=zorder)
        ax.plot(x, means + upper_bounds, color=color_fill, linewidth=1, linestyle=dash, zorder=zorder)
        ax.fill_between(x=x, y1=means - lower_bounds, y2=means + upper_bounds, color=color_fill, zorder=zorder - 1,
                        alpha=1)
        x_points_step = horizon // (horizon // 2)
        x_points = range(x_points_step, horizon, x_points_step)
        common_x_indexes = [i for i, x in enumerate(x) if x in x_points]
        y_points = np.asarray(means)[common_x_indexes]
        points = ax.plot(x_points, y_points, color=line_color, marker=marker, zorder=zorder + 2, linestyle='',
                         markersize=10, lw=3)
        legend_element = Line2D([None], [None], lw=2, label=bandit_name_plot, linestyle=dash, marker=marker,
                                linewidth=2, color=line_color, markersize=10)
        legend_elements.append(legend_element)
    if efficiency_threshold is not None:
        ax.hlines(y=efficiency_threshold, xmin=1, xmax=horizon, linewidth=2, linestyle='--', color='red')
        threshold_legend = Line2D([None], [None], color='red', lw=2, linestyle='--', label='efficiency threshold')
        legend_elements.append(threshold_legend)
    mean_batch_size = np.mean(batch_sizes)
    patch = Patch(facecolor='black', edgecolor=None, label=f'{1 - delta_ci:.0%} confidence interval', alpha=.2)
    legend_elements.append(patch)
    ax.legend(handles=legend_elements, loc=2)
    major_loc = plticker.MultipleLocator(base=2)
    minor_loc = plticker.MultipleLocator(base=1)
    ax.xaxis.set_major_locator(major_loc)
    ax.xaxis.set_minor_locator(minor_loc)
    plt.title(f'Mean efficiencies ; mean batch size: {mean_batch_size:.0f} \n'
              f'{replications} replications')
    if saving_path is None:
        saving_path = f'{saving_prefix}_farmer_efficiencies_R_{replications}_H_{horizon}_A_{100 * alpha:.0f}_100.pdf'
    ax.set_xlabel('time step T')
    ax.set_ylabel('nitrogen efficiency (kg/kg)')
    plt.tight_layout()
    plt.savefig(saving_path)


def plot_multiple_regrets(raw_results, alpha, batch_size, replications, horizon, reward_feature_layout,
                          bandit_names_plot_dic=None, y_logscale=False, y_label=None, x_logscale=False, x_label=None,
                          q_high=.95, q_low=.05, saving_path=None, saving_id=None, plot_saving_prefix='./', title=None,
                          *args, **kwargs):
    bandit_names = [bandit_name for bandit_name in raw_results if bandit_name != 'info']
    bandit_names.sort()
    reward_feature = raw_results['info']['reward_feature']
    raw_regrets = {}
    for bandit_name in bandit_names:
        raw_regret = raw_results[bandit_name]['regret']
        raw_regrets[bandit_name] = np.asarray(raw_regret)
    print('\n#### REGRET RENDERING STAGE ###')
    if saving_path is None:
        saving_path = f'{plot_saving_prefix}_regret_plot_R_{replications}_H_{horizon}_A_' \
                      f'{100 * alpha:.0f}_100.pdf'
    all_colors = np.array([('navy', 'lightblue', 'steelblue'),
                           ('darkorange', 'navajowhite', 'orange'),
                           ('green', 'palegreen', 'forestgreen'),
                           ('deeppink', 'pink', 'hotpink'),
                           ('purple', 'orchid', 'blueviolet'),
                           ])
    dashes = ['dashed', 'solid', 'dotted']
    dashes = itertools.cycle(dashes)
    markers = ['*', 'X', '^']
    markers = itertools.cycle(markers)
    palette_lines = all_colors[:, 0]
    palette_lines = itertools.cycle(palette_lines)
    palette_fill = all_colors[:, 1]
    palette_fill = itertools.cycle(palette_fill)
    fig, ax = plt.subplots()
    legend_elements = []
    batch_sizes = []
    csv_dic = {}
    for index, bandit_name in enumerate(bandit_names):
        if bandit_names_plot_dic is not None:
            bandit_name_plot = bandit_names_plot_dic[bandit_name]
        else:
            bandit_name_plot = bandit_name
        dash = next(dashes)
        raw_regret = raw_regrets[bandit_name]
        rewards = raw_results[bandit_name]['rewards']
        for rewards_ in rewards:
            for rewards__ in rewards_:
                batch_sizes.append(len(rewards__))
        color_fill = next(palette_fill)
        line_color = next(palette_lines)
        marker = next(markers)
        alpha_q = 1
        linewidth = 2
        if index == 0:
            zorder = 2
        else:
            zorder = 1
        quantiles_cum_regret_low = np.quantile(raw_regret, q=q_low, axis=0)
        quantiles_cum_regret_high = np.quantile(raw_regret, q=q_high, axis=0)
        mean_regret = np.mean(raw_regret, axis=0)
        csv_dic[bandit_name] = mean_regret
        x = np.array(range(horizon + 1))
        line = ax.plot(x, mean_regret, color=line_color, linewidth=linewidth, linestyle=dash,
                       zorder=zorder + 2)
        x_points_step = horizon // (horizon // 2)
        x_points = range(x_points_step, horizon, x_points_step)
        common_x_indexes = [i for i, x in enumerate(x) if x in x_points]
        y_points = mean_regret[common_x_indexes]
        points = ax.plot(x_points, y_points, color=line_color, marker=marker, zorder=zorder + 2, linestyle='',
                         markersize=10, lw=3)
        legend_element = Line2D([None], [None], lw=2, label=bandit_name_plot, linestyle=dash, marker=marker,
                                linewidth=linewidth, color=line_color, markersize=10)
        legend_elements.append(legend_element)
        ax.fill_between(x=x, y1=quantiles_cum_regret_low, y2=quantiles_cum_regret_high, color=color_fill,
                        zorder=zorder - 1, alpha=alpha_q)
        ax.plot(x, quantiles_cum_regret_low, color=color_fill, linewidth=1, linestyle=dash, zorder=zorder)
        ax.plot(x, quantiles_cum_regret_high, color=color_fill, linewidth=1, linestyle=dash, zorder=zorder)
    mean_batch_size = np.mean(batch_sizes)
    major_loc = plticker.MultipleLocator(base=2)
    minor_loc = plticker.MultipleLocator(base=1)
    ax.xaxis.set_major_locator(major_loc)
    ax.xaxis.set_minor_locator(minor_loc)
    if x_logscale:
        ax.set_xscale('log')
    if y_logscale:
        ax.set_yscale('log')
    if x_label is None:
        x_label = 'time step T'
        if x_logscale:
            x_label = f'{x_label[:-1]} log(t)'
    ax.set_xlabel(x_label)
    if y_label is None:
        y_label = reward_feature_layout[reward_feature] + ' regret'
    if y_logscale:
        y_label = f'log {y_label}'
    ax.set_ylabel(y_label)
    ax.set_xlim(left=0)
    ax.set_ylim(bottom=0)
    patch = Patch(facecolor='black', edgecolor=None, label=f'{q_low:.02f} to {q_high:.02f} quantile range', alpha=.2)
    legend_elements.append(patch)
    # ax.grid(axis='both')
    ax.legend(handles=legend_elements, loc='best')
    ax.set_xlim(right=horizon)
    if title is None:
        # title = r'Averaged over #{replications} replications for $\alpha={alpha:.0f}$ \\ batch size: {batch_size:.0f}' \
        title = 'Averaged over #{replications} replications for alpha={alpha:.0f}%' \
                '\n mean batch size: {mean_batch_size:.0f}' \
            .format(replications=replications, alpha=100 * alpha, mean_batch_size=np.around(mean_batch_size))
        plt.title(title)
    plt.title(title)
    plt.tight_layout()
    plt.savefig(saving_path)
    plt.close(fig)
    df = pd.DataFrame.from_dict(csv_dic, orient='index', columns=x)
    df.to_csv(f'{saving_path[:-4]}_line.csv')


def plot_action_distribution(raw_results, alpha, batch_size, horizon, replications, n_actions, statistics,
                             gaps, bandit_names_plot_dic=None, efficiency_threshold=None, saving_path_prefix=None):
    sns.set_theme()
    bandit_names = [bandit_name for bandit_name in raw_results if bandit_name != 'info']
    bandit_names.sort()
    all_contexts = [*statistics]
    for bandit_name in bandit_names:
        if bandit_names_plot_dic is not None:
            bandit_name_plot = bandit_names_plot_dic[bandit_name]
        else:
            bandit_name_plot = bandit_name
        ### EXTRACTION ACTIONS BY CONTEXT ###
        actions_by_T = {context: [[] for _ in range(horizon)] for context in all_contexts}
        actions = raw_results[bandit_name]['actions']
        contexts = raw_results[bandit_name]['contexts']
        for replication in range(len(actions)):
            for T in range(horizon):
                actions_rep_T = actions[replication][T]
                context_rep_T = contexts[replication][T]
                for action_index, context in zip(actions_rep_T, context_rep_T):
                    actions_by_T[context][T].append(action_index)
        for context in all_contexts:
            csv_dic = {}
            norm = colors.Normalize()
            gap_values = gaps[context]['all']
            ordered_actions = [action_index for _, action_index in sorted(zip(gap_values, range(n_actions)))]
            norm.autoscale(gap_values)
            action_proportions = []
            ### GET ELIGIBLE ACTIONS ###
            if efficiency_threshold is not None:
                mean_efficiencies = statistics[context]['all']['efficiency']['mean']
                mean_efficiencies = np.asarray(mean_efficiencies)
                eligible_actions = mean_efficiencies >= efficiency_threshold
            ### TURN IT CUMULATIVE ###
            actions_by_T_context = actions_by_T[context]
            for T in range(1, horizon):
                actions_by_T_context[T] = [*actions_by_T_context[T - 1], *actions_by_T_context[T]]
            ### GET CDF ###
            for T in range(horizon):
                count = [0 for _ in range(n_actions)]
                for action_index in actions_by_T_context[T]:
                    count[ordered_actions.index(action_index)] += 1
                count = np.asarray(count)
                count = (count / count.sum()).cumsum() * 100  # to get percentage
                action_proportions.append(count)
            fig, ax = plt.subplots()
            action_proportions_tranpose = list(zip(*action_proportions))
            ### PLOT ###
            for i, action_index in enumerate(ordered_actions):
                gap = gap_values[action_index]
                color = plt.cm.summer(norm(gap))
                hatch = None
                if efficiency_threshold is not None:
                    is_eligible = eligible_actions[action_index]
                    if not is_eligible:
                        hatch = 'x'
                proportions = action_proportions_tranpose[ordered_actions.index(action_index)]
                x = range(1, horizon + 1)
                plt.stackplot(x, proportions, labels=[action_index], zorder=n_actions + 1 - i, hatch=hatch, color=color)
                csv_dic[action_index] = proportions
            plt.title(f'Identification strategy of {bandit_name_plot} ; soil {context} \n {replications} replications')
            major_loc = plticker.MultipleLocator(base=2)
            minor_loc = plticker.MultipleLocator(base=1)
            ax.xaxis.set_major_locator(major_loc)
            ax.xaxis.set_minor_locator(minor_loc)
            ax.yaxis.set_major_formatter(plticker.PercentFormatter())
            ax.set_ylabel('proportion in sampling')
            ax.set_xlabel('time step')
            plt.legend(title='practice index', bbox_to_anchor=(1.04, 0), loc="lower left", borderaxespad=0)
            plt.tight_layout()
            saving_path_prefix_ = f'{saving_path_prefix}_bd_{bandit_name}_c_{context}.pdf'
            plt.savefig(saving_path_prefix_)
            plt.close(fig)
            df = pd.DataFrame.from_dict(csv_dic, orient='index', columns=x)
            df.to_csv(f'{saving_path_prefix_[:-4]}.csv')


if __name__ == '__main__':
    pass
