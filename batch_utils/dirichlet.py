import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
import pdb

if __name__ == '__main__':
    np.random.seed(1234)
    ns = [10, 100]
    colors = ['g', 'b']
    tot_rep = 1
    if tot_rep == 1:
        reps = [1,1]
    else:
        reps = [ns[-1]//ns[0] * tot_rep, tot_rep]
    fig, ax = plt.subplots(figsize=(4.5,2))
    df_dic = {}
    weights = []
    names = []
    for n, color, rep in zip(ns, colors, reps):
        for _ in range(rep):
            weights.extend(np.random.dirichlet(np.ones(n)))
        names.extend([f'{n}' for _ in range(rep*n)])
    df_dic = {'reward number': names, 'weights': weights}
    df = pd.DataFrame.from_dict(df_dic)
    sns.stripplot(y='reward number', x='weights', jitter=.2, size=5, data=df, order=['100', '10'], alpha=.7)
    plt.title('Random weights from Dirichlet distributions')
    # plt.tight_layout()
    plt.savefig('../render/dirichlet_sampling.pdf', bbox_inches='tight')