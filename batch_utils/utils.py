import numpy as np
import os
import signal
import pickle
import logging
import json
import pprint
from gym_dssat_pdi.envs.utils.utils import NumpyDecoder, NumpyEncoder

class handler:
    """
    inspired from https://stackoverflow.com/questions/320232/ensuring-subprocesses-are-dead-on-exiting-python-program
    """

    def __enter__(self):
        os.setpgrp()

    def __exit__(self, type, value, traceback):
        try:
            os.killpg(0, signal.SIGKILL)
        except Exception as e:
            logging.exception(e)

def invert_dict(dict_to_invert):
    inverted_dict = {}
    keys = [*dict_to_invert]
    for key in keys:
        value = dict_to_invert[key]
        inverted_dict.setdefault(value, []).append(key)
    return inverted_dict

def list_to_count_dic(element_list):
    count_dic = {}
    for element in element_list:
        if element not in count_dic:
            count_dic[element] = 0
        count_dic[element] += 1
    return count_dic

def count_dic_to_list(count_dic):
    res_list = []
    for key in [*count_dic]:
        count = count_dic[key]
        res_list.extend([key for _ in range(count)])
    return count_dic

def flatten_dic_into_list(dict_):
    value_list = []
    for key in [*dict_]:
        value = dict_[key]
        value_list.append(value)
    return np.concatenate(value_list, axis=None)  # remove possible extra dimensions

def transpose_ic_dic(ic_dic):
    """
    inspired from https://stackoverflow.com/questions/5558418/list-of-dicts-to-from-dict-of-lists
    """
    return [dict(zip(ic_dic, t)) for t in zip(*[ic_dic[key] for key in [*ic_dic]])]

def save_object(obj, saving_path):
    with open(saving_path, 'wb') as f_:
        pickle.dump(obj, f_, protocol=pickle.HIGHEST_PROTOCOL)

def load_object(loading_path):
    with open(loading_path, 'rb') as f_:
        obj = pickle.load(f_)
    return obj

def fixed_random_sum(n_atoms, n_tot):
    result = np.zeros(n_atoms)
    indexes = range(n_atoms)
    for n in range(n_tot):
        index = np.random.choice(indexes)
        result[index] += 1
    return result

def pickle_to_json(pickle_loading_path, json_saving_path):
    with open(pickle_loading_path, 'rb') as f_:
        dic = pickle.load(f_)
    dic_js = json.dumps(dic, cls=NumpyEncoder).encode('utf-8')
    with open(json_saving_path, 'wb') as f_:
        f_.write(dic_js)

def dic_to_json(dic, saving_path):
    dic_js = json.dumps(dic, cls=NumpyEncoder).encode('utf-8')
    with open(saving_path, 'wb') as f_:
        f_.write(dic_js)

def load_json(json_loading_path):
    with open(json_loading_path, 'rb') as f_:
        dic_js = f_.read()
    dic = json.loads(dic_js, object_hook=NumpyDecoder)
    return dic

def save_pprint_dicts(dict, saving_path='pprint_dicts_save.txt'):
    """
    Utility to save a configuration dictionary in pprint fashion
    @param dict: configuration dictionary to be saved
    @type dict: dictionary
    @param saving_path: path for the configuration to be saved
    @type saving_path: string
    @return: nothing
    @rtype: None
    """
    with open(saving_path, 'wt') as out:
        pprint.pprint(dict, stream=out)

if __name__ == '__main__':
    pass