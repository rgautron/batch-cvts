import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors, cm
import seaborn as sns
import pdb
import os
from gym_dssat_pdi.envs.utils import utils as dssat_utils

plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{amsmath}')


def compute_score_(Y, E, NAE_lim):
    score = Y * (1 - NAE_lim / E)
    return score


if __name__ == '__main__':
    dirs = ['./render', './render/3Dplots']
    for dir in dirs:
        dssat_utils.make_folder(dir)
    try:
        for file in os.scandir('logs'):
            os.remove(file.path)
    except:
        pass
    NAE_lims = [15, 30]
    fig, axes = plt.subplots(ncols=len(NAE_lims), subplot_kw={"projection": "3d"}, figsize=(10, 6))
    Y = np.arange(0, 5000, 50)
    E = np.arange(10, 70, 1)
    Y, E = np.meshgrid(Y, E)
    X_0 = [10, 10]
    cmap = sns.diverging_palette(2, 165, s=80, l=55, as_cmap=True)
    divnorm = colors.TwoSlopeNorm(vmin=-10000, vcenter=0, vmax=4000)
    with sns.axes_style("whitegrid"):
        for NAE_lim, ax in zip(NAE_lims, axes):
            compute_score = lambda Y, E: compute_score_(Y=Y, E=E, NAE_lim=NAE_lim)
            scores = compute_score(Y, E)
            concatenated_scores = np.concatenate(scores, axis=None)
            surf = ax.plot_surface(Y, E, scores, rstride=1, cstride=1, cmap=cmap, linewidth=0, antialiased=False,
                                   norm=divnorm)
            ax.set_facecolor('white')
            ax.set_xlabel(r'$Y^{\pi}-Y^{0}$ (kg/ha)')
            ax.set_ylabel(r'$\text{ANE}^{\pi}$ (kg grain/kg N)')
            ax.set_zlabel(r'$\text{YE}^{\pi}$ (kg/ha)')
            ax.set_zlim(-10000, 4000)
            title = r'$\text{ANE}_{\text{ref}}$' + f' = {NAE_lim:.0f} kg grain/ kg N'
            text = ax.text2D(0.5, 0.95, title, transform=ax.transAxes)
            text.set_ha('center')
            text.set_va('center')
            text.set_fontsize(15)
    plt.subplots_adjust(right=0.85, top=.8)
    cbar_ax = fig.add_axes([0.92, 0.30, 0.02, 0.35])
    fig.colorbar(mappable=cm.ScalarMappable(norm=divnorm, cmap=cmap), cax=cbar_ax, shrink=0.45)
    plt.gca().patch.set_facecolor('0.8')
    # fig.suptitle('Surface plots of Yield Excess')
    plt.savefig(f'./render/3Dplots/3D_plot_ANE.pdf')
