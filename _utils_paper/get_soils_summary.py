from dssatUtils.dssatParsing.dssatParsers import DssatIntegrationParser
import sys, os
python_file_path = "/get_context_example.py"
sys.path.append(os.path.dirname(os.path.expanduser(python_file_path)))
from get_context_example import  make_soil_context_df
import numpy as np
import pdb


if __name__ == '__main__':
    soil_file_path = '../dssat_files/auxiliary_files/IT.SOL'
    soil_feature_df = make_soil_context_df([soil_file_path], max_depth_sloc=30)
    soil_feature_df.to_csv('./output/soils_summary.csv', index=False)