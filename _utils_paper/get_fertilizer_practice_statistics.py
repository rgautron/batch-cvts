import pandas as pd

if __name__ == '__main__':
    practice_statistics_csv_path = '../output/all_actions_dssat_statistics_n_100000.csv'
    df = pd.read_csv(practice_statistics_csv_path, index_col=0)
    n_practices = max([int(index.split('_')[-1]) for index in df.index])
    cultivar = 'IM0001'
    for practice_index in range(n_practices + 1):
        pattern = f'{cultivar}_action_{practice_index}'
        indexes_to_select = [index for index in df.index if pattern in index]
        sub_df = df.loc[indexes_to_select]
        sub_df.to_csv(f'../output/{pattern}_dssat_statistics_n_100000.csv', index=True)
